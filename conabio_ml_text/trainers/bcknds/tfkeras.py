"""
To train a model typically we need 3 components: the model itself, the trainer and the 
configuration the trainer needs to perform the training of the model.

In this script we define this components for a model based in TF keras.

The script includes the three main componens to train a model:
|    - the model: based in TF Keras
|    - the trainer: that uses tensorflow 2.x as backend
|    - the configuration of the execution environmment

All the components subclass from base modules defined in Conabio ML.

In terms of pipeline, we have the following:

|    (Dataset.DatasetType, Model.ModelType, TrainerConfig ) -> 
|    Trainer(Dataset.DatasetType, Model.ModelType, TrainerConfig) -> 
|    Model.ModelType (Trained)
"""
#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import os
import pydash
import shutil

import traceback

import tensorflow as tf

from abc import abstractmethod
from contextlib import redirect_stdout

from pathlib import Path

from conabio_ml.assets import AssetTypes

from conabio_ml.datasets.dataset import Dataset, Partitions, PredictionDataset

from conabio_ml.trainer.model import Model as BaseModel
from conabio_ml.trainer.trainer import Trainer as BaseTrainer
from conabio_ml.trainer.trainer_config import TrainerConfig as BaseTrainerConfig

from conabio_ml_text.trainers.builders import create_learning_rate, create_optimizer, create_loss_function

from conabio_ml.utils.utils import get_and_validate_args
from conabio_ml.utils.logger import get_logger, debugger

from typing import Callable, TypeVar

logger = get_logger(__name__)
debug = debugger.debug

# region Model


class TFKerasBaseModel(BaseModel):
    """
    Base implementation of a model using TFKeras, is based on conabio_ml.trainer.Model.

    Contains the abstract method `create_model` that is implemente only by the specific model.
    See: TODO read_the_docs.

    The abstract methods:
    - `fit`
    - `predict`

    are constrained to the backend used. For every backend check the specific implementation in the
    `conabio_ml_text.trainers.bcknds` module.

    """
    TFKerasModelType = TypeVar('TFKerasModelType', bound='TFKerasBaseModel')

    def __init__(self: BaseModel.ModelType,
                 model_config: dict) -> None:
        assert len(model_config) == 1, \
            (f"The model configuration needs to be placed under the model name key \n"
             f"Check the documentation TODO:")

        self.model_name = [k for k in model_config.keys()][0]
        self.model_config = model_config[self.model_name]

        assert "layers" in self.model_config, \
            (f"The layer configuration of the model has to be defined under the layers property",
             f"Check the documentation TODO:")

        self.layer_config = self.model_config["layers"]
        self.model = None

        super(TFKerasBaseModel, self).__init__(self.model_name)

    @classmethod
    def create(cls: BaseModel.ModelType,
               model_config: dict,
               **kwargs) -> TFKerasModelType:
        """
        Class method to chain in CONABIO ML pipeline

        Parameters
        ----------
        cls : BaseModel.ModelType
            
        model_config : dict
            Definition of the model configuration

        Returns
        -------
        TFKerasModelType
            Model to train
        """
        return cls(model_config=model_config)

    @classmethod
    def load_saved_model(cls,
                         source_path: str,
                         model_config: dict = None,
                         custom_objects: dict = None
                         ) -> TFKerasModelType:
        """Create an instance of a Model from a previously saved model and 
        either a `model_confg` dictionary or a `pipeline_filepath` route.

        Parameters
        ----------
        source_path : str
            The path to a saved model. E.g. a checkpoint path.
        model_config : dict
            A dictionary that contains a model configuration.
            For a complete reference of the classification model parameters,
            follow:
            TODO read_the_docs
        pipeline_filepath : str
            File path of the JSON that defines a filepath
        model_name : str
            Name of the model, it has to be defined inside the 
            pipeline filepath

        Returns
        -------
        Model
            Instance of the created model.
        """
        i = cls(model_config=model_config)
        debug(custom_objects)

        model = tf.keras.models.load_model(source_path,
                                           custom_objects=custom_objects)
        i.model = model

        return i

    def compile_model(self,
                      train_config: dict) -> None:
        """
        Compiles the model using a train_config that needs to implement the optimizer, the loss and the metrics to monitor.
        
        - optimizer: Optimizer of https://www.tensorflow.org/api_docs/python/tf/keras/optimizers or optimizer definition.
        - loss: Loss of https://www.tensorflow.org/api_docs/python/tf/keras/losses or loss definition.
        - metrics: Set of metrics according to [https://www.tensorflow.org/api_docs/python/tf/keras/metrics]  or custom function.
        
        For the implementation

        Parameters
        ----------
        train_config : dict
            It has to define: 
            {
                optimizer: Callable (builders.OPTIMIZER_DEFINITION),

                loss: Callable (builders.LOSS_DEFINITION),

                metrics: Callable (string)
            }
        """
        assert self.name in train_config,\
            f"The model definition must have to be defined under the model name"
        cfg = train_config[self.name]

        assert "optimizer" in cfg,\
            (f"The optimizer options were not found in the model configurationr")

        assert "loss" in cfg,\
            (f"The optimizer options were not found in the model configurationr")

        optimizer = create_optimizer(cfg["optimizer"])
        loss = create_loss_function(cfg["loss"])

        self.model.compile(optimizer=optimizer,
                           loss=loss,
                           metrics=cfg['metrics'])
        debug("Model compiled")

    @abstractmethod
    def fit(self,
            dataset: Dataset.DatasetType,
            train_config: dict,
            execution_config: BaseTrainerConfig.TrainerConfigType) -> None:
        """
        Abstract method:
        Defines the way to fit the model according to the dataset representation
        Check the following resource to guide you TODO: read_the_docs


        Parameters
        ----------
        dataset: Dataset.DatasetType
            Dataset to make the fit process.
            It Must have the following structure
            Columns:
                item, label, partition
            Where item has to contain the vectors to perform
            the training process:
                    item
                X_array, Y_array,
        train_config: dict
            Configuration for the training process
        execution_config: conabio_ml.trainer.trainer_config.TrainerConfig
            Execution config process of the model
        """
        raise NotImplementedError(f"Please define first a model to train")

    @abstractmethod
    def create_model(self) -> TFKerasModelType:
        """
        Defines and return the model definition according to its own backend. 
        The model model should be compatible to compile for the 
        following trainers:

        - TFKerasTrainer

        Returns
        -------
        TFKerasModelType
            Model to be compiled

        Raises
        ------
        NotImplementedError
            When subclass don't define the method
        """
        raise NotImplementedError(f"Please define first a model to train")

    @abstractmethod
    def predict(self: TFKerasModelType,
                dataset: Dataset.DatasetType,
                execution_config: BaseTrainerConfig.TrainerConfigType,
                prediction_config: dict) -> PredictionDataset.DatasetType:
        """
        Defines the method to predict a dataset using an execution_config and prediction_config

        Parameters
        ----------
        self : TFKerasModelType
            Model trained
        dataset : Dataset.DatasetType
            Dataset to predict
        execution_config : BaseTrainerConfig.TrainerConfigType
            Configuration of the environment of the model
        prediction_config : dict
            Arguments for the prediction process

        Returns
        -------
        PredictionDataset.DatasetType
            Dataset with the predictions performed for the model

        Raises
        ------
        NotImplementedError
            General exception
        """
        # The dataset relies on representations to perform
        # the training process, defined in the fit method.
        # To predict, we'll also delegate the prediction process
        # to the specific model handling.

        raise NotImplementedError(f"Predict implementation not founc")

    # reporter methods

    def reporter(self: BaseModel.ModelType,
                 dest_path: str,
                 process_args: dict,
                 data_to_report: dict = {},
                 **kwargs):
        """
        Reports the default resources of the model built, whose are:
        - model_summary: Definition of the model layers
        - model_history: History of the training process

        Parameters
        ----------
        self : BaseModel.ModelType
            
        dest_path : str
            Destination path to save the resources
        process_args : dict
            Optional arguments of the process
        data_to_report : dict, optional
            Data reported from subclass report methods, by default {}

        Returns
        -------
        dict
            Chained super class

        Raises
        ------
        err
            When an error appers we silently report with the logger
        """
        try:
            Path(dest_path).mkdir(exist_ok=True)

            dest_model_path = os.path.join(dest_path, f'{self.name}.txt')
            dest_model_history_path = os.path.join(
                dest_path, f'{self.name}_history.json')

            with open(dest_model_path, 'w') as f:
                with redirect_stdout(f):
                    try:
                        self.model.summary()
                    except:
                        logger.error(f"The model is not build yet, if you're using the Trainer-Model"
                                     f"schema consider reporting the model once it's already trained")
                        return super(TFKerasBaseModel, self).reporter(dest_path,
                                                                      process_args,
                                                                      data_to_report,
                                                                      **kwargs)

            with open(dest_model_history_path, 'w') as hist_file:
                try:
                    hist = self.model.history.history
                    json.dump(hist, hist_file)
                except:
                    logger.error(f"We cannot retrive the model history.")
                    return super(TFKerasBaseModel, self).reporter(dest_path,
                                                                  process_args,
                                                                  data_to_report,
                                                                  **kwargs)

            data_to_report = {
                "model_summary": {"asset": dest_model_path,
                                  "type": AssetTypes.FILE},
                "model_history": {"asset": dest_model_history_path,
                                  "type": AssetTypes.FILE}
            }

            return super(TFKerasBaseModel, self).reporter(dest_path,
                                                          process_args,
                                                          data_to_report,
                                                          **kwargs)
        except Exception as err:
            logger.error(err)
            raise err

# endregion

# region TrainerConfig

"""
Default strategies that can be used in the training process.
"""

MIRROREDSTRATEGY = "mirrored_strategy"

STRATEGY_DEFINITIONS = {
    MIRROREDSTRATEGY: {
        'devices': {
            'type': list,
            'default': None
        }
    }
}

DEFAULT_STRATEGIES = {
    MIRROREDSTRATEGY: lambda args: tf.distribute.MirroredStrategy(
        **get_and_validate_args(args,
                                STRATEGY_DEFINITIONS[MIRROREDSTRATEGY],
                                ignore_invalid=True)
    )
}

"""
Default callbacks to monitor in the training process
"""
CHECKPOINT_CALLBACK = 'checkpoint'
TENSORBOARD_CALLBACK = 'tensorboard'
EARLYSTOPPING_CALLBACK = "early_stopping"

CALLBACK_DEFINITION = {
    CHECKPOINT_CALLBACK: {
        'filepath': {
            'type': str,
            'optional': False
        },
        'monitor': {
            'type': str,
            'default': 'val_loss'
        },
        'verbose': {
            'type': int,
            'default': 1
        },
        'save_best_only': {
            'type': bool,
            'default': True
        },
        'save_weights_only': {
            'type': bool,
            'default': False
        },
        'mode': {
            'type': str,
            'default': 'auto'
        },
        'save_freq': {
            'type': str,
            'default': "epoch"
        }
    },
    TENSORBOARD_CALLBACK: {
        'log_dir': {
            'type': str,
            'optional': False
        },
        'histogram_freq': {
            'type': int,
            'default': 0
        },
        'profile_batch': {
            'type': int,
            'default': 2
        },
        'write_graph': {
            'type': bool,
            'default': True
        },
        'write_images': {
            'type': bool,
            'default': False
        },
        'embeddings_freq': {
            'type': int,
            'default': 0
        },
        'update_freq': {
            'type': str,
            'default': 'epoch'
        }
    },
    EARLYSTOPPING_CALLBACK: {
        'monitor': {
            'type': str,
            'default': 'val_loss'
        },
        'verbose': {
            'type': int,
            'default': 1
        },
        'min_delta': {
            'type': float,
            'default': 0
        },
        'patience': {
            'type': int,
            'default': 2
        },
        'mode': {
            'type': str,
            'default': 'auto'
        },
        'restore_best_weights': {
            'type': bool,
            'default': True
        },
    }
}

DEFAULT_CALLBACKS = {
    CHECKPOINT_CALLBACK: lambda args: tf.keras.callbacks.ModelCheckpoint(
        **get_and_validate_args(args,
                                CALLBACK_DEFINITION[CHECKPOINT_CALLBACK],
                                ignore_invalid=True)
    ),
    TENSORBOARD_CALLBACK: lambda args: tf.keras.callbacks.TensorBoard(
        **get_and_validate_args(args,
                                CALLBACK_DEFINITION[TENSORBOARD_CALLBACK],
                                ignore_invalid=True)
    ),
    EARLYSTOPPING_CALLBACK: lambda args: tf.keras.callbacks.EarlyStopping(
        **get_and_validate_args(args,
                                CALLBACK_DEFINITION[EARLYSTOPPING_CALLBACK],
                                ignore_invalid=True)
    )
}


class TFKerasTrainerConfig(BaseTrainerConfig):
    """
    Trainer_config defines the environment where the model will be trained. Is based on 
    BaseTrainerConfig
    """

    def __init__(self: BaseTrainerConfig.TrainerConfigType,
                 config: dict) -> None:
        """
        Creates the variables need to configure the execution options

        Parameters
        ----------
        self : BaseTrainerConfig.TrainerConfigType
            
        config : dict
            Configuration of the execution environment, defines:{
                "strategy": STRATEGY_DEFINITIONS | None,
                "callbacks": dict(CALLBACK_DEFINITION)
            }
        """
        
        try:
            self.strategy_name = [k for k in config["strategy"].keys()][0] \
                if config["strategy"] \
                else None
            self.strategy_config = config["strategy"]
            self.callbacks_config = config.get("callbacks", None)

            if self.callbacks_config is None:
                logger.info((f"Callback information not provided, by default "
                             f"{CHECKPOINT_CALLBACK} and {TENSORBOARD_CALLBACK} callbacks are added."))
        except Exception as ex:
            logger.exception(ex)

    @classmethod
    def create(cls: BaseTrainerConfig.TrainerConfigType,
               config: dict = {
                   "callbacks": {
                       CHECKPOINT_CALLBACK: {
                           "filepath": "./"
                       },
                       TENSORBOARD_CALLBACK: {
                           "log_dir": "./"
                       }
                   },
                   "strategy": MIRROREDSTRATEGY
               }) -> BaseTrainerConfig.TrainerConfigType:
        """
        Creates an instance of a class KerasTrainerConfig that defines
        the execution paramaters of the training environment

        Parameters
        ----------
        checkpoint_dir: str
            Directory where checkpoints and event logs will be written to
        checkpoint_dir: str
            Directory where logs will be written to

        Returns
        -------
        TFKerasTFBackendTrainerConfig
            Backend execution paramaters
        """
        return cls(config)

    def build_callbacks(self, config:dict) -> list:
        """
        Builds the callbacks according to the user defined dictionary 
        self.callbacks_config

        Returns
        -------
        list
            [List of callbacks defined by the param self.callbacks_config]
        """
        callbacks = pydash.chain(self.callbacks_config.items())\
            .map(lambda cb: DEFAULT_CALLBACKS.get(cb[0])(cb[1]))\
            .value()

        return callbacks

    def build_strategy(self) -> tf.distribute.Strategy:
        """
        Builds the strategy to train a model using the parameter self.strategy_name
        defined by the user in the TrainerConfig config dictionary.

        If the strategy name is not in the DEFAULT_STRATEGIES, it will be built using
        the users own definition

        Returns
        -------
        tf.distribute.Strategy
            Strategy to train a model
        """
        try:
            if self.strategy_name is None:
                strategy = None
            elif self.strategy_name in DEFAULT_STRATEGIES:
                strategy = DEFAULT_STRATEGIES\
                    .get(self.strategy_name)(self.strategy_config[self.strategy_name])
            else:
                strategy = self.strategy_config[self.strategy_name]

            debug(f"{self.strategy_name} Strategy built")

            return strategy
        except Exception as ex:
            logger.exception(f"The Strategy cannot be built properly")
            logger.exception(ex)
            raise
# endregion

# region Trainer


class TFKerasTrainer(BaseTrainer):
    @classmethod
    def train(cls,
              dataset: Dataset.DatasetType,
              model: TFKerasBaseModel.TFKerasModelType,
              execution_config: BaseTrainerConfig.TrainerConfigType,
              train_config: dict) -> TFKerasBaseModel.TFKerasModelType:
        """
        Performs the model training, it follows the following procedure:
            - Creates the strategy using execution_config
            - Asks for the model architechture to model
            - If any strategy is defined the model is built using it
            - Performs the fit process of model with (dataset, train_config,mexecution_config)

        Parameters
        ----------
        dataset : Dataset.DatasetType
            Dataset to train the model
        model : TFKerasBaseModel.TFKerasModelType
            Model to train
        execution_config : BaseTrainerConfig.TrainerConfigType
            Environment config to train the model
        train_config : dict
            Additional params to train

        Returns
        -------
        TFKerasBaseModel.TFKerasModelType
            Model trained
        """
        try:
            assert model.get_type() is BaseModel.ModelType, \
                "The model sent has to be of ModelType type"
            assert execution_config.get_type() is BaseTrainerConfig.TrainerConfigType, \
                "The environment config sent has to be of TrainerConfigType type"
            assert dataset.get_type() is Dataset.DatasetType, \
                "The dataset sent has to be of DatasetType type"

            # The class TFKerasTrainerConfig defines the strategy to use
            # for the training of the model
            strategy: tf.distribute.Strategy = execution_config.build_strategy()

            debug(f"Building the model with {model.model_config}")
            if not strategy:
                # At this time model is still None
                # We ask in the model the way to create it
                # it returns a TFKeras.ModelType
                model.model = model.create_model(model.model_config)
            else:
                with strategy.scope():
                    model.model = model.create_model(model.model_config)

            model.compile_model(train_config=train_config)

            # In a TFKeras model, the model itself defines the way to be trained.
            # The CONABIO ML API defines an abstract way to perform a training, in
            # other kinds of model an external entity performs the training of the model.
            # Check the documentation of the model training in TODO:
            model.fit(dataset=dataset,
                      train_config=train_config,
                      execution_config=execution_config)

            return model
        except Exception as ex:
            logger.exception(ex)

    @classmethod
    def on_finish(cls,
                  process_name: str,
                  on_finish_data: dict = {},
                  **kwargs):
        """
        This method is intended to be chained by a child class to report
        data after executed.

        Parameters
        ----------
        process_name : str
            Name of the process who is executing this method
        on_finish_data : dict, optional
            Data reported by a child class, by default {}

        Returns
        -------
        dict
            Data reported for a child class
        """

        try:
            pipeline = kwargs.get("pipeline", None)

            if pipeline:
                temp = kwargs.get("tfkerastrainerconfig", None)
                config = temp.callbacks_config

                try:
                    pipeline_path = pipeline.path

                    checkpoints_dir = config[CHECKPOINT_CALLBACK].get(
                        "filepath")
                    tb_dir = config[TENSORBOARD_CALLBACK].get("log_dir")

                    shutil.copytree(checkpoints_dir, os.path.join(
                        pipeline_path, os.path.basename(checkpoints_dir)
                    ))
                    shutil.copytree(tb_dir, os.path.join(
                        pipeline_path, os.path.basename(tb_dir)
                    ))
                except Exception as ex:
                    logger.warning(ex)

            super(TFKerasTrainer, cls).on_finish(process_name=process_name,
                                                 on_finish_data=on_finish_data,
                                                 **kwargs)
        except Exception as ex:
            raise ex
# endregion
