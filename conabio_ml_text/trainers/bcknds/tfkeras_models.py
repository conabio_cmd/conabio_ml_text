#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
import pandas as pd
import tensorflow as tf

import traceback

from abc import abstractmethod

from conabio_ml.datasets.dataset import Dataset, Partitions
from conabio_ml.trainer.trainer_config import TrainerConfig as BaseTrainerConfig


from conabio_ml_text.datasets.dataset import PredictionDataset
from conabio_ml_text.preprocessing.transform import Transform
from conabio_ml_text.trainers.bcknds.tfkeras import TFKerasBaseModel
from conabio_ml_text.utils.constraints import TransformRepresentations as TR


from conabio_ml.utils.logger import get_logger, debugger

from typing import Callable, Generator, List, TypeVar

logger = get_logger(__name__)
debug = debugger.debug


class TFKerasRawDataModel(TFKerasBaseModel):

    def fit(self,
            dataset: Dataset.DatasetType,
            train_config: dict,
            execution_config: BaseTrainerConfig.TrainerConfigType) -> None:
        """
        This model relies in two types of data representations previously defined
        in the dataset, whose have to be defined in the dataset.representations property of the
        dataset:
        - dataset.representations[TransformRepresentations.DATA_GENERATORS]
        - dataset.representations[TransformRepresentations.TF_DATASET]


        Parameters
        ----------
        dataset: Dataset.DatasetType
            The dataset must have the following structure
            Columns:
                [item, label, partition]
            And has to contain either dataset.representations[DATA_GENERATORS] or
            dataset.representations[TF_DATASET] representations

        train_config: dict
            Configuration for the training process
        execution_config: conabio_ml.trainer.trainer_config.TrainerConfig
            Execution config process of the model
        """
        logger.info("Starting fitting process")

        assert TR.DATA_GENERATORS in dataset.representations \
            or TR.TF_DATASET in dataset.representations, \
            (f"This model relies its fit process on the `dataset` or `generators` "
             f"representation of the data, please check TODO: ")

        cfg = train_config[self.name]

        train_type = cfg.get(
            'representation', TR.DATA_GENERATORS)
        self.train_representation = train_type
        assert train_type in dataset.representations,\
            (f"The dataset representation choosen is not defined in the train configuration")

        {
            TR.DATA_GENERATORS: self.fit_with_generator,
            TR.TF_DATASET: self.fit_with_dataset
        }.get(train_type)(dataset,
                          train_config,
                          execution_config)

        logger.info(f"Model fitting finished")

    # region generator training

    def fit_with_generator(self,
                           dataset: Dataset.DatasetType,
                           config: dict,
                           execution_config: BaseTrainerConfig.TrainerConfigType) -> None:
        """
        Using the representation `TransformRepresentations.DATA_GENERATORS`, we perform the model
        training process.

        The representation `TransformRepresentations.DATA_GENERATORS` must have to be established 
        priorly. 

        Parameters
        ----------
        dataset : Dataset.DatasetType
            Dataset to train the model.
            The train process optionally can use validation data if the dataset was priorly partitioned.]
        config : dict, optional
            Options for the training process.
        execution_config : TrainerConfig.TrainerConfigType, optional
            Options of the environment to perform the training process.

        Raises
        ------
        err
            [Thrown if config or execution_config does not contains the mandatory values.]
        ex
            [General exception.]
        """
        try:
            val = None
            model_cfg = config[self.name]
            batch_size = model_cfg["batch_size"]

            data = dataset.representations[TR.DATA_GENERATORS]
            train_data,  val_data = data.get(
                Partitions.TRAIN), data.get(Partitions.VALIDATION)

            temp = next(train_data())
            aut_outshape = tuple([tf.shape(t) for t in temp])

            debug(f"Automatic output shapes: {aut_outshape}")
            assert train_data,\
                (f"Training data not found. To train as datagenerator, the dataset representation "
                 f"{TR.DATA_GENERATORS} has to de set")

            train = tf.data.Dataset.from_generator(train_data,
                                                   (tf.float32, tf.float32),
                                                   output_shapes=aut_outshape
                                                   )
            if np.issubdtype(type(batch_size), int):
                train = train.batch(batch_size)

            if val_data:
                val = tf.data.Dataset.from_generator(val_data,
                                                     (tf.float32, tf.float32),
                                                     output_shapes=aut_outshape
                                                     )

                if np.issubdtype(type(batch_size), int):
                    val = val.batch(batch_size)

            model_callbacks = execution_config.build_callbacks(config)

            self.model.fit(train,
                           validation_data=val,
                           epochs=model_cfg["epochs"],
                           callbacks=model_callbacks)
        except KeyError as err:
            logger.error(
                f"The key {err} cannot be found in the configuration of the model")
            logger.error(err)
            raise err
        except Exception as ex:
            logger.error(
                f"The model {self.name} cannot be trained succesfully")
            logger.error(ex)
            raise ex

    def get_generator_data(self,
                           generator: Generator) -> List[np.array]:
        """
        Extracts the information of a data generato to perform the prediction process.

        Parameters
        ----------
        generator : Generator
            Data generator to extract

        Returns
        -------
        List[np.array]
            X, Y
        """
        try:
            X = []
            Y = []

            for item in generator():
                x, y = item
                X.append(x)
                Y.append(y)

            X = np.vstack(X)
            Y = np.vstack(Y)

            return X, Y

        except Exception as ex:
            logger.error(ex)
            raise
    # endregion

    # region dataset training

    def fit_with_dataset(self,
                         dataset: Dataset.DatasetType,
                         config:dict,
                         execution_config=BaseTrainerConfig.TrainerConfigType) -> None:
        """
        Using the representation `TransformRepresentations.TF_DATASET`, we perform the model
        training process.

        The representation `TransformRepresentations.TF_DATASET` must have to be established 
        priorly. 

        Parameters
        ----------
        dataset : Dataset.DatasetType
            Dataset to train the model.
            The train process optionally can use validation data if the dataset was priorly partitioned.
        config : dict, optional
            Options for the training process.
        execution_config : TrainerConfig.TrainerConfigType, optional
            Options of the environment to perform the training process

        Raises
        ------
        err
            Thrown if config or execution_config does not contains the mandatory values.
        ex
            General exception.
        """
        try:
            val = None
            model_cfg = config[self.name]
            batch_size = model_cfg["batch_size"]

            data = dataset.representations[TR.TF_DATASET]

            if not batch_size:
                log.warning((f"Batch size not established. \n"
                             f"Empty batch size are often used when you control the amount of samples "
                             f"in data generator. BE SURE YOU DOUBLE CHECK THIS OPTION"))

            train_data,  val_data = data.get(
                Partitions.TRAIN), data.get(Partitions.VALIDATION)

            assert train_data,\
                (f"Training data not found. To train as daraset, the dataset representation "
                 f"{TR.DATASET} has to de set")

            train = train_data.batch(batch_size)

            if val_data:
                val = val_data.batch(batch_size)

            model_callbacks = execution_config.build_callbacks(config)

            self.model.fit(train,
                           validation_data=val,
                           epochs=model_cfg["epochs"],
                           callbacks=model_callbacks)
        except KeyError as err:
            logger.error(
                f"The key {err} cannot be found in the configuration of the model")
            logger.error(err)
            raise err
        except Exception as ex:
            logger.error(
                f"The model {self.name} cannot be trained succesfully")
            logger.error(ex)
            raise ex

    def get_tfdataset_data(self,
                           tfdataset: tf.data.Dataset) -> List[np.array]:
        """
        Return the data contained in the a tensorflow dataset to make the prediction
        process

        Parameters
        ----------
        tfdataset : tf.data.Dataset
            Dataset to extract

        Returns
        -------
        List[np.array]
            X, Y
        """
        try:
            x, y = zip(*tfdataset.as_numpy_iterator())

            X = np.vstack(x)
            Y = np.vstack(y)

            return X, Y
        except Exception as ex:
            logger.error(ex)
            raise
    # endregion

    def predict(self: TFKerasBaseModel.TFKerasModelType,
                dataset: Dataset.DatasetType,
                execution_config: BaseTrainerConfig.TrainerConfigType,
                prediction_config: dict = {
                    "prediction_fn": None,
                    "representation": TR.DATA_GENERATORS,
                    "sparse_predictions": False,
                    "ommit_uniques": False
                }) -> PredictionDataset.DatasetType:
        """
        Makes the prediction process of a Model over a Dataset to produce a PredictionDataset

        Parameters
        ----------
        self : TFKerasBaseModel.TFKerasModelType
            Model to make predictions
        dataset : Dataset.DatasetType
            Dataset to make predictions. It searchs for the `test` partition, if it is not found
            it predict over the complete Dataset
        execution_config : BaseTrainerConfig.TrainerConfigType
            Environment options of the model
        prediction_config : dict, optional
            Additional arguments for the prediction process. It contains:

                - prediction_fn: Function to convert the samples, if None it uses np.argmax

                - representation: Representation to use

                - sparse_predictions: If True it returns all the scores for label for each sample

            , by default 
            { 
                "prediction_fn": None, 

                "representation": TR.DATA_GENERATORS, 

                "sparse_predictions": False 

            }

        Returns
        -------
        PredictionDataset.DatasetType
            Dataset with predictions and scores
        """
        assert TR.DATA_GENERATORS in dataset.representations \
            or TR.TF_DATASET in dataset.representations, \
            (f"This model relies its predict process on the `tensors` or `generators` "
             f"representation of the data, please check TODO: ")

        REPRESENTATION_MAPPER = {
            TR.DATA_GENERATORS: self.get_generator_data,
            TR.TF_DATASET: self.get_tfdataset_data
        }

        try:
            converter = prediction_config.get("prediction_fn", None)
            sparse_labels = prediction_config.get("sparse_predictions", False)
            ommit_uniques = prediction_config.get("ommit_uniques", False)

            data_representation = prediction_config\
                .get("representation", TR.DATA_GENERATORS)

            assert data_representation in dataset.representations,\
                (f"The selected representation is not present in the dataset. Use the "
                 f"prediction_config.representation option to choose one valid representation")

            partition = Partitions.TEST

            if dataset.is_partitioned():
                assert Partitions.TEST in dataset.get_partitions_names(), \
                    logger.warning((f"The dataset is reported as partitioned, nevertheless "
                                    f"{Partitions.TEST} partition is not found"))
            else:
                partition = None

            data = dataset.representations[data_representation]
            # Rows in the partition no matter repeated "items"
            # Always len(rows) >= len(dataset_items)
            rows = dataset.get_partition(partition)

            # Ordinals of the indices in the dataset for unique_values of the item columns
            # Are grouped by "item"
            if ommit_uniques:
                unique_ixs = np.array(range(len(rows)))
            else:
                unique_ixs = np.array(
                    list(
                        map(lambda x: x[0], rows.groupby(
                            'item').indices.values())
                    )
                )
            # The data established in the representation-partition
            data = data[partition]

            # X, Y obteined from data
            # Should have to be len(x_test) == len(dataset_items)
            x_test, y_test = REPRESENTATION_MAPPER\
                .get(data_representation)(data)

            unique_x_test = x_test[unique_ixs]
            unique_y_test = y_test[unique_ixs]

            # Preds are in one_hot for the class
            # Son a [N X M] matrix of len -> len(dataset_items) == len(x_test)
            preds = self.model.predict(unique_x_test)

            labelmap = dataset.params.get("labelmap",
                                          {ix: label for ix, label in enumerate(range(preds.shape[1]))})

            preds_df = pd.DataFrame(columns=["id", "label", "score"])
            label_ixs = np.array(list(labelmap.keys()))

            assert preds.shape[1] == len(label_ixs), \
                (f"Labelmcap does not correspond with the output of the model"
                 f"labelmap: {labelmap} != Model outputs: {len(label_ixs)}")

            labelmap_values = np.repeat([label_ixs], preds.shape[0], axis=0)

            if sparse_labels:
                # When using sparse labels option we only return all the scores for each
                # label-item
                # So, we have a preds_df of size len(unique_ixs) * len(labels)
                pred_ix, labels = np.where(preds > 0)

                ixs = unique_ixs[pred_ix]
                scores = preds[pred_ix, labels]
                labels = labelmap_values[pred_ix, labels]
            else:
                if converter:

                    conv_preds = converter(unique_y_test,
                                           preds,
                                           execution_config,
                                           prediction_config)
                    # conv_preds are also a [N X M] matrix of length = len(preds) of {0, 1}

                    # This converte the predctions to int, so, according to the method
                    # is len(pred_ix) == len(labels) >= len(conv_preds)
                    pred_ix, labels = np.where(conv_preds > 0)

                    empty_preds = np.where(np.sum(conv_preds, axis=1) == 0)[0]

                    # When the model/pred_eval_fn is not working well, we have 0s in columns.
                    # So, it's a problem for evluation.
                    # We only throw a warning
                    if len(empty_preds) > 0:
                        logger.error((f"The prediction process performed by the converter {converter.__name__}"
                                      f"is returning less predictions than samples in x_test. "
                                      f"X_TEST_SIZE:{len(x_test)}, NON_ZERO_PREDICTION_SIZE: {len(conv_preds) - len(empty_preds)}"))

                    # We can have more tha one prediction for index, so len(ixs) != len(unique_ixs)
                    ixs = unique_ixs[pred_ix]
                    scores = preds[pred_ix, labels]
                    labels = labelmap_values[pred_ix, labels]
                else:
                    ixs = unique_ixs
                    labels = np.argmax(preds, axis=1)
                    scores = np.max(preds, axis=1)

            preds_df["id"] = rows.iloc[ixs].index
            preds_df["score"] = scores
            preds_df["partition"] = partition

            preds_df["label"] = np.array(
                list(map(lambda x: labelmap[x],
                         labels))
            )

            temp = PredictionDataset.from_dataset(dataset,
                                                  preds_df)

            debug("Assets")
            debug(f"Test dataset size {len(rows)} <= "
                  f"Result data size {len(temp.data)}")
            debug(f"Prediction size {len(preds)} <= "
                  f"Converted prediction size {len(labels)}")
            debug(f"Unique items size {len(unique_ixs)} "
                  f"== Result data unique items {len(temp.data['item'].unique())}")

            return temp

        except Exception as ex:
            logger.error(ex)
            raise

    @abstractmethod
    def create_model(self) -> TFKerasBaseModel.TFKerasModelType:
        """
        Abstract method:
        Defines the model definition and returns the model to compile 
        in the following trainers:

        - TFKerasTrainer

        Returns:
            TFKerasModelType: Model to be compiled
        """
        raise NotImplementedError(f"Please define first a model to train")
