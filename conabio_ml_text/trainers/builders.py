#!/usr/bin/env python
# -*- coding: utf-8 -*-
# TODO: Script documentation
import traceback

import tensorflow as tf

from conabio_ml_text.utils.constraints import LearningRates as LR
from conabio_ml_text.utils.constraints import Optimizers as OPTS
from typing import Callable, Union

from conabio_ml.utils.utils import get_and_validate_args
from conabio_ml.utils.logger import get_logger, debugger

logger = get_logger(__name__)
debug = debugger.debug

# region loss function builder

""" Loss function definitions """

LOSS_DEFINITION = {

}

LOSS_MAPPER = {

}

def create_loss_function(loss: Union[Callable, dict]) -> tf.keras.losses.Loss:
    """
    Builds a loss function using a LOSS_DEFINITION or if is already a callable
    returns it

    Parameters
    ----------
    loss : Union[Callable, dict]
        Loss function definition

    Returns
    -------
    tf.keras.losses.Loss
        Loss function
    """
    try:
        if isinstance(loss, dict):
            debug("We create loss function")
        else:
            debug(f"The loss function cannot be built from default options, "
                  f"we return completely {str(loss.__class__)}")
            return loss
    except Exception:
        logger.error(
            f"An error occurred while building the loss function")
        raise
# endregion


# region optimizer builder
OPTIMIZER_DEFINITION = {
    OPTS.ADAM: {
        'learning_rate': {
            'type': [dict, Callable],
            'optional': False
        },
        'beta_1': {
            'type': float,
            'default': 0.9
        },
        'beta_2': {
            'type': float,
            'default': 0.999
        },
        'epsilon': {
            'type': float,
            'default': 1e-7
        },
        'amsgrad': {
            'type': bool,
            'default': False
        },
        "clipnorm": {
            'type': float,
            'default': None
        },
        "clipvalue": {
            'type': float,
            'default': None
        },
        'name': {
            'type': str,
            'default': "Adam"
        }
    }
}

OPTIMIZER_MAPPER = {
    OPTS.ADAM: lambda args: tf.keras.optimizers.Adam(
        **args
    )
}


def create_optimizer(optimizer: Union[Callable, dict]) -> tf.keras.optimizers.Optimizer:
    """
    Builds an optimizer function using a OPTIMIZER_DEFINITION or if is already a callable
    returns it

    Parameters
    ----------
    optimizer : Union[Callable, dict]
        Optimizer definition

    Returns
    -------
    tf.keras.optimizers.Optimizer
        Optimizer function
    """
    try:
        if isinstance(optimizer, dict):
            optimizer_name = [k for k in optimizer.keys()][0]

            assert optimizer_name in OPTIMIZER_MAPPER, \
                (f"Only default optimizers can be configured "
                 f"using a config dictionary. Check supported optimizers in TODO:")

            optimizer_config = optimizer[optimizer_name]
            optimizer_args = get_and_validate_args(optimizer_config,
                                                   OPTIMIZER_DEFINITION[optimizer_name],
                                                   ignore_invalid=False)
            debug(
                f"Building the {optimizer_name} with {optimizer_args}")

            opt = OPTIMIZER_MAPPER.get(optimizer_name)(optimizer_args)

            return opt
        else:
            debug(f"The optimizer function cannot be built from default options, "
                  f"we return {str(optimizer.__class__)}")
            return optimizer
    except Exception:
        logger.error(
            f"An error occurred while building the optimizer")
        raise
# endregion

# region learning rate builder


LR_DEFINITION = {
    LR.EXPONENTIAL_LR: {
        'initial_learning_rate': {
            'type': float,
            'default': 0.002
        },
        'decay_steps': {
            'type': int,
            'optional': False
        },
        'decay_rate': {
            'type': float,
            'default': 0.95
        },
        'staircase': {
            'type': bool,
            'default': True
        },
        'name': {
            'type': str,
            'default': "ExponentialDecay"
        }
    }
}

LR_MAPPER = {
    LR.EXPONENTIAL_LR: lambda args: tf.keras.optimizers.schedules.ExponentialDecay(
        **args)
}


def create_learning_rate(learning_rate: Union[Callable, dict],
                         learning_rate_name: str = None) -> tf.keras.optimizers.schedules.LearningRateSchedule:
    """
    Builds a learning rate function using a LR_DEFINITION or if is already a callable
    returns it

    Parameters
    ----------
    learning_rate : Union[Callable, dict]
        Learning rate definition
    learning_rate_name : str, optional
        Learning rate name, by default None

    Returns
    -------
    tf.keras.optimizers.schedules.LearningRateSchedule
        Learning rate function
    """
    try:
        if isinstance(learning_rate, dict):
            assert learning_rate_name in LR_MAPPER, \
                (f"Only default learning rate functions can be configured "
                 f"using a config dictionary. Check supported LR function in TODO:")

            lr_args = get_and_validate_args(learning_rate,
                                            LR_DEFINITION[learning_rate_name],
                                            ignore_invalid=False)
            debug(f"Building {learning_rate_name} with {lr_args}")

            lr = LR_MAPPER.get(learning_rate_name)(lr_args)

            return lr
        else:
            debug(f"The learning rate function cannot be built from default options, "
                  f"we return {str(learning_rate.__class__)}")
            return learning_rate
    except Exception:
        logger.error(
            f"An error occurred while building the optimizer")
        raise

# endregion
