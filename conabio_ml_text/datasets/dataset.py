"""
It contains all the particular handlings of Dataset to be considered when
managing text tasks.
"""
#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import pandas as pd
import json
import pydash
import uuid
import copy
import multiprocessing
import threading
import types
import traceback

from typing import List, Callable,  Union
from pathlib import Path

from concurrent.futures import ThreadPoolExecutor

import conabio_ml.utils.dataset_utils as utils

from conabio_ml_text.conabio_ml.conabio_ml.datasets.dataset import Partitions

from conabio_ml.datasets import Partitions, Dataset as DS
from conabio_ml.datasets import PredictionDataset as PDS

from conabio_ml.utils.logger import get_logger, debugger
from conabio_ml.utils.utils import Chained

from conabio_ml_text.utils.constraints import TransformRepresentations as TR

logger = get_logger(__name__)
debug = debugger.debug

NUM_PROCESSES = multiprocessing.cpu_count() - 1
# region Dataset


class Dataset(DS):
    """
    The Dataset of type Text contains functionality useful for
    NLP tasks.

    It also handles the representation of data build from preprocessing
    methods on its variable representations

    Parameters
    ----------
    DS : DatasetType
        Type of the class to be chained in the Pipeline

    representations: [dict]
        Contains the representations that could be built in the preprocessing/transform
        processes. These representations are:
            - vocab
            - dataset
            - data_generators
    """

    def __init__(self,
                 data: pd.DataFrame,
                 info: dict,
                 **kwargs) -> None:
        self.representations = {
            TR.VOCAB: None
        }

        super(Dataset, self).__init__(data, info, **kwargs)

    # region

    # region loading

    __lock = threading.Lock()

    @classmethod
    def build_from_json(cls,
                        json_path,
                        categories,
                        group_fields,
                        multilabel,
                        **kwargs) -> DS.DatasetType:
        """ Builds a filtered dataset from the data loads from json file
          and retrieves it as dataframa

        Parameters
        ----------
        json_path: str
          Path to a JSON file that contains the information of a collection.
        destination_folder : str
          Destination folder to stores auxiliar data
        categories : list
          List of categories to filter

        Returns
        -------
        Tuple
          (pd.DataFrame, dict)
        """
        data = json.load(open(json_path, 'r'))
        anns = data["annotations"]
        items = data["items"]

        items_to_anns = {}

        for ann in anns:
            item_id = ann["item_id"]

            if item_id not in items_to_anns:
                items_to_anns[item_id] = []

            items_to_anns[item_id].append(ann)

        if categories is None:
            return pd.DataFrame(items), data["info"]
        else:
            if len(items_to_anns) > 0:
                df_data = []

                for item in items:
                    key = item["id"]
                    item_anns = items_to_anns.get(key)

                    if not item_anns:
                        temp = item
                        temp["label"] = None

                        df_data.append(temp)
                    else:
                        for item_ann in item_anns:
                            temp = copy.copy(item)
                            temp["label"] = item_ann["category_id"]

                            df_data.append(temp)

                temp = pd.DataFrame(df_data)

                if len(categories) > 0:
                    temp = temp.loc[temp['label'].isin(categories)]

                return temp, data["info"]

    # endregion

    # region FROM methods

    @classmethod
    def from_folder(cls,
                    source_path: Union[str, Path],
                    extensions: List[str] = [""],
                    recursive: bool = True,
                    label_by_folder_name: bool = True,
                    split_by_folder: bool = False,
                    include_id: bool = False,
                    info: dict = {},
                    item_reader: Callable = None,
                    **kwargs) -> DS.DatasetType:
        """Creates a `Dataset` from a folder structure. It bypass the item reader function
        to the default `filesystem_writer`

        Parameters
        ----------
        source_path : str
            Path of a folder to be loaded and converted into a Dataset object.
        extensions : list of str, optional
            List of extensions to seek files in folders. [""] to seek all files in folders, by default[""]
        recursive : bool
            Whether or not to seek files also in subdirectories, by default True
        label_by_folder_name : bool
            Whether or not you want to label each item of the dataset according to the name of the folder that contains it, by default True
        split_by_folder : bool, optional
            Split the dataset from the directory structure (default is False).

            For this option, directories must be in the following structure:
            
                - source_path
                - `train`:class folders
                - `test`:class folders
                - `validation`:class folders
                
        include_id : bool, optional
            Wheter or not to include an id for each register, by default False
        info : dict, optional
            Information of the folder structure, by default {}
        item_reader : Callable, optional
            Function that receives the file to process. The function needs to contain the following signature:
                
                filesystem_writer(item:str, dest_path:str)

            The resulting value will be stores in the `item` column of the dataset

        Returns
        -------
        Dataset
            Instance of the created `Dataset`
        """
        def default_reader(x): return (yield x)
        source_path = source_path if isinstance(source_path, Path)\
            else Path(source_path)

        assert source_path.is_dir(),\
            f"{source_path} is not a valid folder name."

        extensions = list(set([x.lower() for x in extensions]))
        _files = []

        _files += utils.get_all_files(source_path, seek_extension=extensions)

        debug(f"{len(_files)} found files for extensions {source_path}")

        reader = item_reader or default_reader

        processes_results = []

        # We add this line to debug with pdb, since all the process is
        # made in parallel
        if debugger.bp or False:
            cls.process_file_item(file_info=_files[0],
                                  source_path=source_path,
                                  file_reader=reader,
                                  label_by_folder_name=label_by_folder_name,
                                  split_by_folder=split_by_folder,
                                  include_id=include_id)

        with ThreadPoolExecutor(max_workers=NUM_PROCESSES) as executor:
            for f in _files:
                try:
                    processes_results.append(executor.submit(cls.process_file_item,
                                                             file_info=f,
                                                             source_path=source_path,
                                                             file_reader=reader,
                                                             label_by_folder_name=label_by_folder_name,
                                                             split_by_folder=split_by_folder,
                                                             include_id=include_id))
                except Exception as ex:
                    logger.error(traceback.print_exc())
                    raise ex

        res = pydash.chain(processes_results)\
            .map(lambda x: x.result())\
            .flatten()\
            .value()

        data = pd.DataFrame(res)
        instance = cls(data, info, **kwargs)

        if split_by_folder:
            instance._split_by_column(column="partition", delete_column=True)

        return instance

    @classmethod
    def process_file_item(cls, file_info: dict,
                          source_path: Path,
                          file_reader: Callable,
                          label_by_folder_name: bool = True,
                          split_by_folder: bool = False,
                          include_id: bool = False):
        """
        This functions is intended to process only ONE file provided by from_folder method.

        Parameters
        ----------
        file_info : dict
            Filepath defined as 
                {
                    "title":Title of the file, 
                    "path": Path of the file
                }
        source_path : Path
            Path of a folder to be loaded and converted into a Dataset object.
        file_reader : Callable
            Function to process the file.
        label_by_folder_name : bool, optional
            Whether or not you want to label each item of the dataset accordingto the name of the folder that contains it, by default True
        split_by_folder : bool, optional
            Split the dataset from the directory structure.

            For this option, directories must be in the following structure::
                - source_path
                - `train`: class folders
                - `test`: class folders
                - `validation`: class folders
            
            , by default True
        include_id : bool, optional
            Wheter or not to include an id for each register, by default False
        """
        def exc_wrapper(x):
            raise Exception(f"Unsupported type of item {type(item)}")

        SUPPORTED_READER_TYPES = {
            str: lambda x: [x],
            types.GeneratorType: lambda x: [*x],
            list: lambda x: x,
            None: exc_wrapper
        }

        try:
            res = []
            path = Path(file_info["path"]).relative_to(source_path)
            label = path

            if label_by_folder_name:
                if split_by_folder:
                    label = "/".join(path.split("/")[1:])

            item_filepath = source_path / path / file_info["title"]
            item = file_reader(item_filepath)

            unsupported = SUPPORTED_READER_TYPES[None]
            unrolled_item = SUPPORTED_READER_TYPES.get(type(item),
                                                       unsupported)(item)
            for item in unrolled_item:
                if isinstance(item, dict):
                    temp = item
                elif isinstance(item, str):
                    temp = {
                        "item": item
                    }
                else:
                    raise Exception(
                        f"Unsupported inner type of item {type(item)}")

                if label_by_folder_name:
                    temp["label"] = str(label)

                if include_id:
                    temp["id"] = str(uuid.uuid4())

                if split_by_folder:
                    partition = path.split("/")[0]
                    if partition not in Partitions.NAMES:
                        raise Exception(f"Files have to be contained in folders called "
                                        f"{Partitions.NAMES} for split_by_folder option")
                    temp["partition"] = partition
                    logger.debug(f"Folder for partition {partition} was found")

                res.append(temp)

            return res
        except Exception as ex:
            logger.error(
                f"An error ocurred while processing the item {item_filepath}")
            raise ex

    @classmethod
    def default_reader(cls,
                       filepath: str) -> str:
        """
        Default implementation to read text files persisted in the filesystem

        Parameters
        ----------
        filepath : str
            Filepath of the resource to read

        Returns
        -------
        str
            File content
        """
        with cls.__lock:
            with open(filepath, mode="r") as _f:
                temp = _f.read()

        yield temp

    # endregion

    # region split

    def sequential_split(self: DS.DatasetType,
                         train_perc: float = 0.8,
                         test_perc: float = 0.2,
                         val_perc: float = 0) -> DS.DatasetType:
        """
        Splits the dataset in `train`, `test` and `validation`
        partitions using sequential sections of the data.

        Parameters
        ----------
        train_perc : float, optional
            Train set percentage. Should have to be between 0 and 1, by default 0.8
        test_perc : float, optional
            Test set percentage. Should have to be between 0 and 1, by default 0.2
        val_perc : float, optional
            Validation set percentage. Should have to be between 0 and 1, by default 0

        Returns
        -------
        DS.DatasetType
            Instance of the Dataset
        """

        perc_sum = train_perc + test_perc + val_perc
        assert perc_sum > 0.99 and perc_sum < 1.01, 'Partitions have to SUM 1'

        partitions = {Partitions.TRAIN: train_perc,
                      Partitions.TEST: test_perc}
        if val_perc > 0.:
            partitions[Partitions.VALIDATION] = val_perc

        start = 0.
        ordinals = len(self.data)
        ixs = self.data.index

        for part, val in partitions.items():
            if start + val >= 1:
                partitions_ixs = ixs[int(start * ordinals):]
                partition_rows = self.data[partitions_ixs]

                self.set_partition(part, partition_rows)
            else:
                partitions_ixs = ixs[int(start * ordinals):
                                     int((start + val) * ordinals)]
                partition_rows = self.data[partitions_ixs]

                self.set_partition(part, partition_rows)
                start += val

        return self

    # endregion

    # region reporter methods

    def reporter(self,
                 dest_path: str,
                 process_args: dict,
                 report_fns: Callable = [],
                 **kwargs):
        """
        This method is chained after the completion of a process related to a Dataset.
        It saves all the assets need to build a report of the top-level process with the
        variable data_to_report.

        Parameters
        ----------
        dest_path : str
            Destination path where the resources will be stored
        process_args : dict
            Arguments of the process defined
        report_fns : Callable, optional
            Function reference for specific user functions], by default []

        Returns
        -------
        Dataset.DatasetType
            Chain to the super class
        """
        data_to_report = {}
        for fn in report_fns:
            temp = fn(self, dest_path, **process_args)
            data_to_report.update(temp)

        data_to_report = {}

        return super(Dataset, self).reporter(dest_path,
                                             process_args,
                                             data_to_report)

    # endregion

    # region aux methods

    # endregion

# endregion


class PredictionDataset(PDS):
    @classmethod
    def from_dataset(cls,
                     dataset: DS.DatasetType,
                     preds: pd.DataFrame,
                     only_preds: bool = True,
                     **kwargs) -> PDS.DatasetType:
        """
        Creates a PredictionDataset from a Dataset using the predictions of the model trained

        Parameters
        ----------
        dataset : DS.DatasetType
            Dataset of reference for the predictions
        preds : pd.DataFrame
            DataFrame with the predictions to attach to the Dataset
        only_preds : bool, optional
            If true, it creates the dataset only with the predictions and ommits all other fields, by default True
        sparse_dataset : bool, optional
            If true, it creates multiple rows according to the predictions sent, by default False

        Returns
        -------
        PredictionDataset.DatasetType
            Instance of PredictionDataset
        """
        assert isinstance(preds, pd.DataFrame),\
            (f"Predictions format not supported")

        data = dataset.data

        dataset.data["score"] = 0.

        ixs = preds.id

        if only_preds:
            data = dataset.data.loc[ixs]

        columns_to_replace = ["label", "score", "partition"]
        for col in columns_to_replace:
            with Chained():
                data[col] = list(preds[col])

        i = PredictionDataset(data, dataset.info)

        return i

    def reporter(self,
                 dest_path: str,
                 process_args: dict,
                 report_fns: Callable = [],
                 **kwargs):
        """
        This method is chained after the completion of a process related to a Dataset.

        It saves all the assets need to build a report of the top-level process with the variable data_to_report.

        Parameters
        ----------
        dest_path : str
            Destination path where the resources will be stored
        process_args : dict
            Arguments of the process defined
        report_fns : Callable, optional
            Function reference for specific user functions, by default []

        Returns
        -------
        PredictionDataset.DatasetType
            Chain to the super class
        """
        data_to_report = {}

        for fn in report_fns:
            temp = fn(self, dest_path, **process_args)
            data_to_report.update(temp)

        return super(PredictionDataset, self).reporter(dest_path,
                                                       process_args,
                                                       data_to_report)
