"""
This script contains methods intended to report some aspects of the
dataset.

In general, all methods that will be executed in the reporter interface should have the
following template:

method_name(dataset, dest_path, **kwargs):
    data_to_report = {}

    return data_to_report

"""

#!/usr/bin/env python
# -*- coding: utf-8 -*-
import traceback

import pandas as pd

from pathlib import Path

from conabio_ml.datasets.dataset import Dataset

from conabio_ml.utils.logger import get_logger, debugger

logger = get_logger(__name__)
debug = debugger.debug


def word_count(dataset: Dataset.DatasetType,
               dest_path: str,
               **kwargs):
    """
    Creates and stores in the {dest_path}/word_count.csv filepath
    a file that contains the count of words in the dataset ordered by ocurrence
    in format (word, count_word, partition)

    Parameters
    ----------
    dataset : Dataset.DatasetType
        Dataset to report
    dest_path : str
        Destination path of the word_count.csv file
    Keyword Arguments:
        split_by_partitions : bool 
            If True, the file will contain the column partition that indicates the corresponding partition 

    Returns
    -------
    dict
        Dictionary with the destination file of the CSV file, as follows:  `{word_count: destination_file}`
    """
    filepath = Path(f"{dest_path}/word_count.csv")
    data_to_report = {"word_count": str(filepath)}

    try:
        res = pd.DataFrame()

        partitions = dataset.get_partition_names()

        for partition in partitions:
            rows = dataset.get_partition(partition)

            words = rows["item"].apply(lambda x: x.split())
            words_count = pd.Series(words).value_counts()
            temp = pd.DataFrame({"word": words_count.index,
                                 "count_word": words_count.values,
                                 "partition": partition})

            res = res.append(temp)

        res.to_csv(filepath)

        return data_to_report
    except Exception as ex:
        logger.error(ex)
        return {}
