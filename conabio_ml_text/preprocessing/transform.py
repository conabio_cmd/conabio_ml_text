"""
This script contains the most common Transforms applied to a Dataset to be trained in a 
ML model.

Transform subclass of PreProcessing.So, as same as, Preprocessing classes always work with static 
methods to modify the Dataset class. 

In terms of pipeline, we have the following:
Dataset.DatasetType -> Prerprocessing.method(Dataset.DatasetType) -> Dataset.DatasetType

Finally, all the resouces produced as a result of the transformation process HAVE to be stored
in the `dataset.representations` property of Dataset. The default transformations are currently enlisted
in the `conabio_ml_text.utils.constraints.TransformRepresentations` class.
"""
#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import json

import traceback

import random as rnd
import numpy as np
import pandas as pd
import tensorflow as tf

from pandas.api.types import is_string_dtype, is_numeric_dtype

from typing import Callable, Iterable, List, Generator, Tuple, Union
from pathlib import Path

from conabio_ml.datasets import Dataset, Partitions
from conabio_ml.preprocessing import PreProcessing
from conabio_ml_text.preprocessing import BasePreprocessing as PreProc

from conabio_ml_text.preprocessing.preprocessing import PreProcessing
from conabio_ml_text.preprocessing.preprocessing import Tokens

from conabio_ml.utils.logger import get_logger, debugger

from conabio_ml_text.utils.constraints import TransformRepresentations as TR, LabelTypes
from conabio_ml_text.utils.utils import poolify

logger = get_logger(__name__)
debug = debugger.debug


class Transform(PreProcessing):
    """
    Defines the common methods to transform a Dataset to be suitable into a ML training phase.

    """
    # region as methods
    # All of these methods save the specific representation built in the
    # `representations` property of the Dataset.
    # If Pipeline manager won't be used check the transform_to section

    @staticmethod
    def as_dataset(dataset: Dataset.DatasetType,
                   vocab: list,
                   shuffle: bool = True,
                   categorical_labels: bool = True,
                   transform_args: dict = {
                       "pad_length": 0,
                       "vocab_size": 0,
                       "unk_token": Tokens.UNK_TOKEN
                   }) -> Dataset.DatasetType:
        """
        Builds the transformation of a Dataset to TFKeras Dataset and stores it into 
        the `dataset.representations["TransformRepresentations.TF_DATASET"]` property

        Parameters
        ----------
        dataset : Dataset.DatasetType
            Dataset to transform
        vocab : list
            Vocabulary to convert the Dataset to tensors
        shuffle : bool, optional
            If True, the train partition will be shuffle every time it will be queried,  by default True
        categorical_labels : bool, optional
            If True, the labels are transformed into one_hot representation, by default True
        transform_args : dict, optional
            Arguments to be sent to the transform function, contains:
            
            the paddding length of the sentence
            
            the length to limit the vocabulary size
            
            the Token to handle unk words], 
            
            by default 
            { 
            
                "pad_length": 0, 
            
                "vocab_size": 0, 
            
                "unk_token": Tokens.UNK_TOKEN 
        
            }

        Returns
        -------
        Dataset.DatasetType
            Dataset transformed
        """
        try:
            partition_datasets = {}

            assert "labelmap" in dataset.params, \
                (f"The labelmap to convert labels is not defined, "
                 f"make sure it was automatocally built or defined in the dataset")

            debug(
                f"Labelmap to convert `label` column for dataset: {dataset.params['labelmap']}")

            vocab_size = transform_args.get("vocab_size", 0)
            pad_length = transform_args.get("pad_length", 0)

            vocab, word_dict = Transform.__build_vocab(dataset,
                                                       vocab,
                                                       vocab_size)

            vocab = np.array(vocab)

            partitions = dataset.get_partitions_names()
            partitions = partitions if len(partitions) > 0 else [None]

            for partition in partitions:

                data = dataset.get_partition(partition)

                items, labels = Transform\
                    .transform_dataset(dataset,
                                       data.index,
                                       categorical_labels)

                X, Y = Transform.transform_to_tensor(items,
                                                     labels,
                                                     word_dict=word_dict,
                                                     transform_args=transform_args)

                ds = tf.data.Dataset.from_tensor_slices((X, Y))
                shuffle_partiton = False if partition != Partitions.TRAIN else shuffle

                if shuffle_partiton:
                    ds = ds.shuffle(len(X) - 1)

                partition_datasets[partition] = ds

            dataset.representations[TR.TF_DATASET] = partition_datasets
            dataset.register_preprocessing_op(TR.TF_DATASET, {
                "vocab_size": len(vocab),
                "padding": pad_length
            })

            return dataset
        except Exception as ex:
            logger.exception(ex)
            raise

    @staticmethod
    def as_data_generator(dataset: Dataset.DatasetType,
                          vocab: list,
                          shuffle: bool = True,
                          categorical_labels: bool = True,
                          data_generator: Callable = None,
                          transform_args: dict = {
                              "pad_length": 0,
                              "vocab_size": 0,
                              "unk_token": Tokens.UNK_TOKEN
                          }) -> Dataset.DatasetType:
        """
        Builds the transformation of a Dataset to a generator of tensors, and stores it into 
        the `dataset.representations["TransformRepresentations.DATA_GENERATORS"]` property.

        It also, contains the option to bypass the default generator to a custom one.

        Parameters
        ----------
        dataset : Dataset.DatasetType
            Dataset to transform
        vocab : list
            Vocabulary to convert the Dataset to tensors
        shuffle : bool, optional
            If True, the train partition will be shuffle every time it will be queried, by default True
        categorical_labels : bool, optional
            If True, the labels are transformed into one_hot representation, by default True
        data_generator : Callable, optional
            If function handler, a custom data generator will be executed to transform,

            If None, __build_data_generator methid will be used.
            
            The custom method has to have the same signature as __build_data_generator, 
            
            by default None
        transform_args : dict, optional
            
            Arguments to be sent to the transform function, contains:

            the paddding length of the sentence
            
            the length to limit the vocabulary size
            
            the Token to handle unk words], 
            
            by default 
            { 
                "pad_length": 0, 
            
                "vocab_size": 0, 
            
                "unk_token": Tokens.UNK_TOKEN 
            
            }

        Returns
        -------
        Dataset.DatasetType
            Dataset transformed
        """

        try:
            partition_generators = {}

            assert "labelmap" in dataset.params, \
                (f"The labelmap to convert labels is not defined, "
                 f"make sure it was automatocally built or defined in the dataset")

            debug(
                f"Labelmap to convert `label` column for dataset: {dataset.params['labelmap']}")

            vocab_size = transform_args.get("vocab_size", 0)
            pad_length = transform_args.get("pad_length", 0)

            vocab, word_dict = Transform.__build_vocab(dataset,
                                                       vocab,
                                                       vocab_size)

            vocab = np.array(vocab)

            partitions = dataset.get_partitions_names()
            partitions = partitions if len(partitions) > 0 else [None]

            for partition in partitions:
                data = dataset.get_partition(partition)

                items, labels = Transform\
                    .transform_dataset(dataset,
                                       data.index,
                                       categorical_labels)

                X, Y = Transform.transform_to_tensor(items,
                                                     labels,
                                                     word_dict=word_dict,
                                                     transform_args=transform_args)

                shuffle_partiton = False if partition != Partitions.TRAIN else shuffle

                if data_generator:
                    datagen = data_generator(dataset,
                                             X,
                                             Y,
                                             shuffle=shuffle_partiton,
                                             **transform_args)
                else:
                    datagen = Transform.__build_data_generator(X,
                                                               Y,
                                                               shuffle=shuffle_partiton)
                partition_generators[partition] = datagen

            dataset.representations[TR.DATA_GENERATORS] = partition_generators
            dataset.register_preprocessing_op(TR.DATA_GENERATORS, {
                "vocab_size": len(vocab),
                "padding": pad_length
            })

            return dataset
        except Exception as ex:
            logger.exception(ex)
            raise

    # endregion

    # region transform_to methods
    # These methods actually return the representations to set into the
    # as_X methods to be managed by the Pipeline
    @staticmethod
    def transform_to_tensor(X: Iterable[List],
                            Y: Iterable,
                            word_dict: dict = None,
                            transform_args: dict = {
                                "pad_length": 0,
                                "unk_token": Tokens.UNK_TOKEN}) -> Tuple:
        """
        Converts the Dataset to X, Y tensors.

        Parameters
        ----------
        X : Iterable[List]
            Items to convert. They have to be a list of words for every row.
        Y : Iterable
            Labels to convert.They have to be an iterable.
        word_dict : dict, optional
            Vocabulary to use in the transfor process, by default None
        transform_args : dict, optional
            Argument to send to the transform function. It contains:

            The padding of the sentence,

            The unknown token], by default 
            { 
                "pad_length": 0, 
            
                "unk_token": Tokens.UNK_TOKEN
            
            }

        Returns
        -------
        Tuple [np.array]
            X , Y
        """

        assert isinstance(X, Iterable),\
            "The object to process must to be an iterable"

        fn_args = {"word_dict": word_dict,
                   "pad_length": transform_args.get("pad_length", 0),
                   "unk_token": transform_args.get("unk_token", Tokens.UNK_TOKEN), }

        X = poolify(X, fn_args, Transform.sentence_to_tensor)

        X = np.vstack(X)
        X = X.astype("int32")

        if Y is not None:
            Y = np.array(Y)

        return X, Y

    # endregion

    # region auxiliar methods
    @staticmethod
    def transform_dataset(dataset: Dataset.DatasetType,
                          sample_ixs: pd.core.indexes,
                          categorical_labels: bool) -> Tuple[pd.Series, pd.Series]:
        """
        Converts the dataset into an Iterable of items (tensors) ans labels (integers, one-hot representations)

        Parameters
        ----------
        dataset : Dataset.DatasetType
            Dataset to transform
        sample_ixs : pd.core.indexes
            Indexes of the samples to convert
        categorical_labels : bool
            If True: it convert the labels to one-hot representation

        Returns
        -------
        Tuple[pd.Series, pd.Series]
            [
                items: sentences converted to tensors

                labels: labels converted to int or one-hot
            ]
        """
        try:
            labels = None

            temp = dataset.data.loc[sample_ixs]

            if dataset.is_labeled():
                inverse_label_map = {v: k for k,
                                     v in dataset.params['labelmap'].items()}
                label_type = dataset.data["label"]

                assert is_numeric_dtype(label_type) \
                    or is_string_dtype(label_type) \
                    or isinstance(label_type.iloc[0], str), \
                    f"Not supported label mapping for label type: {label_type.dtypes}"

                labels = temp["label"]

                if is_string_dtype(label_type):
                    labels = dataset.data.loc[sample_ixs, "label"]\
                        .apply(lambda x: inverse_label_map[x])
                else:
                    labels = dataset.data.loc[sample_ixs, "label"]

                if categorical_labels:
                    labels = tf.keras.utils.to_categorical(labels)

            items = temp["item"].apply(lambda x: x.split()).values

            return items, labels
        except Exception as ex:
            logger.error(ex)
            raise

    @staticmethod
    def __build_vocab(dataset: Dataset.DatasetType,
                      vocab_asset: Union[list, Path, str] = None,
                      vocab_size: int = 0) -> Tuple[List, dict]:
        """
        Builds the vocabulary using an asset to be considered as follows:

        - If list[str]: The vocab is already defined an is only limited to vocab_size

        - If str|Path: The vocab is considered as a file with one word per line. Is  converted to a list and limited to vocab_size

        - If None: The vocab will be built with the dataset using the __vocab_from_dataset method with the size sent

        Parameters
        ----------
        dataset : Dataset.DatasetType
            Dataset to use in the process.
        vocab_asset : Union[list, Path, str], optional
            Asset to consider in the vocab building, by default None
        vocab_size : int, optional
            Size of the vocabulary. If 0, the vocab is not limited, by default 0

        Returns
        -------
        Tuple[List, dict]
            [
                Vocabulary , 

                Inverse vocabulary
            ]
        """
        try:
            vocab = vocab_asset

            if vocab:
                assert isinstance(vocab, (list, Path, str)), \
                    "Unsupported vocabulary definition"

                if isinstance(vocab, (str, Path)):
                    with open(vocab) as _f:
                        vocab = _f.read()
                        vocab = vocab.split("\n")

            else:
                if dataset.representations[TR.VOCAB] is None:
                    logger.warning((f"The vocabulary is neither defined as params nor established as "
                                    f"dataset representation, we will try to build it using the dataset"))

                    try:
                        vocab = Transform.__vocab_from_dataset(dataset,
                                                               vocab_size=vocab_size)
                    except Exception as ex:
                        logger.exception(
                            (f"Dataset partition {Partitions.TRAIN} cannot be found."))
                        raise
                else:
                    vocab = dataset.representations[TR.VOCAB]

            if vocab_size > 0:
                vocab = vocab[0:vocab_size]

            # Once we limitthe unrolled vocabulary we set uo the
            # TR.VOCAB representation
            dataset.representations[TR.VOCAB] = vocab

            inv_vocab = {v: ix for ix, v in enumerate(vocab)}
            return vocab, inv_vocab
        except Exception as ex:
            logger.error(ex)
            raise

    @staticmethod
    def __vocab_from_dataset(dataset: Dataset.DatasetType,
                             vocab_size: int) -> List[str]:
        """
        Builds the vocabulary using the dataset with the default options of
        PreProcessing.build_vocab

        Parameters
        ----------
        dataset : Dataset.DatasetType
            Dataset to use in the process
        vocab_size : int
            Size of the vocabulary

        Returns
        -------
        List[str]
            List of tokens
        """
        try:
            dict_data = dataset.get_partition(Partitions.TRAIN)
            assert "item" in dict_data,\
                (f"Field item is required to automatic vocabulary counstruction. ",
                 f"Set the vocabulary manually.")

            vocab = PreProcessing.build_vocab(dict_data["item"],
                                              vocab_size=vocab_size)
            return vocab
        except Exception as ex:
            logger.exception(ex)
            raise

    @staticmethod
    def __build_data_generator(X: np.array,
                               Y: np.array,
                               shuffle: bool = True,
                               **kwargs) -> Generator:
        """
        Default implementation to convert a matrix into a data generator

        Parameters
        ----------
        X : np.array
            X items
        Y : np.array
            Y items
        shuffle : bool, optional
            If true, the data will be shuffled, by default True

        Returns
        -------
        generator
            The data generato handler

        Yields
        -------
        Generator
            X, Y
        """
        _X = X
        _Y = Y
        data_ixs = list(range(len(_X)))

        if np.isnan(_X).any():
            logger.warning(f"X array contains NaN values")

        if _Y:
            if np.isnan(_Y).any():
                logger.warning(f"Y array contains NaN values")

        def data_generator():
            if shuffle:
                rnd.shuffle(data_ixs)

            for ix in data_ixs:
                yield (_X[ix], None if not _Y else _Y[ix])

        return data_generator

    @staticmethod
    def sentence_to_tensor(process_args={
            "word_dict": {},
            "pad_length": 0,
            "type_padding": "prepend",
            "unk_token": Tokens.UNK_TOKEN},
            sentence: list = []) -> List[int]:
        """
        Converts a sentence into a tensor representation. It considers the following options:

            word_dict: dict of words with word:ix

            pad_length: Padding length of the resulting tensor

            type_padding: Type of padding. If preprend -> sentence pad_tokens, other case -> pad_tokens sentence

            unk_token: Token for unknown words.

        Parameters
        ----------
        process_args : dict, optional
            Argument to send to the convert function, by default 
            { 
                "word_dict": {}, 

                "pad_length": 0, 

                "type_padding": "prepend", 

                "unk_token": Tokens.UNK_TOKEN

            }
        sentence : list, optional
            List of words, by default []

        Returns
        -------
        List[int]
            Tensor representation
        """
        word_dict = process_args["word_dict"]
        pad_length = process_args.get("pad_length", 0)
        unk_token = process_args.get("unk_token", Tokens.UNK_TOKEN)
        prepend = process_args.get("type_padding", "prepend") == "prepend"

        word_to_token = []

        for token in sentence:
            temp = word_dict.get(token, word_dict[unk_token])
            word_to_token.append(temp)

        tokens = np.array(word_to_token)

        if pad_length > 0:
            mask = np.zeros((pad_length))
            tokens = tokens[-pad_length:]
            ln = len(tokens)

            if ln != pad_length:
                init = 0 if prepend else -ln - 1
                end = ln if prepend else -1
                mask[init:end] = tokens
                tokens = mask

        return tokens

    # endregion
