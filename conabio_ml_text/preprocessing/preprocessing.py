"""
This script contains the base template to make any preprocessing for text data, and
the default implementation for the preprocessing process. It also defines the default
tokens to be used in text preprocessing.

Preprocessing classes always work with static methods since they change/works over
a Dataset class. 

In terms of pipeline, we have the following:
Dataset.DatasetType -> Prerprocessing.method(Dataset.DatasetType) -> Dataset.DatasetType

So, we can considered all of this methods as transformations of the Dataset itself.

Finally, all the resouces produced as a result of the transformation process HAVE to be stored
in the `dataset.representations` property of Dataset. The default transformations are currently enlisted
in the `conabio_ml_text.utils.constraints.TransformRepresentations` class.
"""
#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import re
import string
import multiprocessing

import traceback

from abc import abstractmethod

from collections import Counter, defaultdict

from nltk.tokenize import sent_tokenize, word_tokenize
from nltk.corpus import stopwords

from typing import Callable, Iterable, List
from multiprocessing import Pool
from concurrent.futures import ThreadPoolExecutor

from conabio_ml.datasets import Dataset, Partitions
from conabio_ml.preprocessing import PreProcessing as PreProc

from conabio_ml.utils.logger import get_logger, debugger
from conabio_ml.utils.utils import Chained
from conabio_ml_text.utils.utils import poolify
from conabio_ml_text.utils.constraints import TransformRepresentations as TR


logger = get_logger(__name__)
debug = debugger.debug

NUM_PROCESSES = multiprocessing.cpu_count() - 1


class Tokens():
    """
    It contains the default Tokens used in the preprocessing/transform processes.
    """
    PAD_TOKEN = '[PAD]'
    EOS_TOKEN = '[EOS]'
    UNK_TOKEN = '[UNK]'
    NUM_TOKEN = '[NUM]'


class BasePreprocessing(PreProc):
    """
    BasePreprocessing contains the default methods to be performed on preprocesing 
    text data.  

    It also defines the common method for any subclass: the preprocess method.
    """

    @staticmethod
    def preprocess_data(data: Iterable,
                        preprocess_args: dict = {
                            "processes": NUM_PROCESSES,
                            "func_args": {}
                        },
                        preprocess_fn: Callable = None) -> Iterable:
        """
        Accomplish in a thread parallel way the preprocessing over an iterable data.

        Parameters
        ----------
        data : Iterable
            Data to preprocess. It has to be a compatible structure with
            pool.map parallelization method.
        preprocess_args : dict, optional
            Defines the number of threads to parallelize and the arguments  for the preprocess function,  by default 
            { 
                "processes": NUM_PROCESSES, 
                
                "func_args": {} 
            }
        preprocess_fn : Callable, optional
            Preprocess function to parallelize, by default None

        Returns
        -------
        Iterable
            Data processed
        """
        try:
            processes = preprocess_args.get("processes", NUM_PROCESSES)
            apply_args = preprocess_args.get("func_args", {})
            fn = preprocess_fn or DefaultProcessor().preprocess

            if debugger.bp or False:
                fn(item=data[0], preproc_args=apply_args)

            processes_results = []
            with ThreadPoolExecutor(max_workers=processes) as executor:
                for d in data:
                    try:
                        temp = executor.submit(fn,
                                               item=d,
                                               preproc_args=apply_args)
                        processes_results.append(temp)
                    except Exception:
                        log.error(
                            f"An error ocurred while processing the item {d}")
                        log.error(traceback.print_exc())
                        raise

            data = [*map(lambda x: x.result(), processes_results)]

            return data
        except Exception as ex:
            logger.exception(ex)
            raise

    @staticmethod
    @abstractmethod
    def preprocess(dataset: Dataset.DatasetType,
                   preprocess_args: dict = {
                       "fields": ["item"],
                       "func_args": {},
                   },
                   build_vocab: bool = False,
                   vocab_args: dict = {
                       "vocab_size": 0,
                       "field": "item",
                       "default_tokens": [Tokens.PAD_TOKEN, Tokens.UNK_TOKEN]
                   },
                   vocab_builder: Callable = None,
                   preprocess_fn: Callable = None):
        """
        Abstract method to be defined for any subclass of BasePreprocessing

        Parameters
        ----------
        dataset : Dataset.DatasetType
            Dataset to preprocess
        preprocess_args : dict, optional
            Arguments to be sent to the preprocessing method, by default 
            { 
                "fields": ["item"], 
                
                "func_args": {}
            }
        build_vocab : bool, optional
            If True, it builds the vocabulary, by default False
        vocab_args : dict, optional
            Arguments to be sent to vocab_builder method,  by default 
            { 
                "vocab_size": 0, 
            
                "field": "item", 
            
                "default_tokens": [Tokens.PAD_TOKEN, Tokens.UNK_TOKEN] 
            
            }
        vocab_builder : Callable, optional
            Method to build the vocabulary,  by default None
        preprocess_fn : Callable, optional
            Function to preprocess the Dataset, by default None

        Raises
        ------
        NotImplementedError
            
        """
        raise NotImplementedError()


class PreProcessing(BasePreprocessing):
    """
    Default class to apply preprocessing operations to text
    """
    @staticmethod
    def preprocess(dataset: Dataset.DatasetType,
                   preprocess_args: dict = {
                       "fields": ["item"],
                       "func_args": {},
                   },
                   build_vocab: bool = False,
                   vocab_args: dict = {
                       "vocab_size": 0,
                       "fields": ["item"],
                       "default_tokens": [Tokens.PAD_TOKEN, Tokens.UNK_TOKEN]
                   },
                   vocab_builder: Callable = None,
                   preprocess_fn: Callable = None) -> Dataset.DatasetType:
        """
        Preprocesses the Dataset using the DefaultProcessor class, excepting another method in the preprocess_fn was indicated. 

        It also builds the vocabulary using the field sent, using build_vocab, excepting another  method in the vocab_builder was indicated. 

        Parameters
        ----------
        dataset : Dataset.DatasetType
            Dataset to preprocess
        preprocess_args : dict, optional
            Arguments to be sent to the preprocessing method,  by default 
            { 
                "fields": ["item"], 
                "func_args": {} 
            }
        build_vocab : bool, optional
            If True, it builds the vocabulary with the method vocab_builder and stores it  in the `dataset.representations["vocab"]` property, by default False
        vocab_args : dict, optional
            Arguments to be sent to vocab_builder method, by default 
            { 
                "vocab_size": 0, 
            
                "field": "item", 
            
                "default_tokens": [Tokens.PAD_TOKEN, Tokens.UNK_TOKEN] 
            
            }
        vocab_builder : Callable, optional
            Method to build the vocabulary. If None is sent, it uses the  build_vocab method, by default None
        preprocess_fn : Callable, optional
            If a method handler is sent, this method is used to preprocess the Dataset. If None, DefaultProcessor is used, by default None

        Returns
        -------
        Dataset.DatasetType
            Dataset proprocessed
        """
        fields = preprocess_args.get("field", ["item"])
        def_tokens = preprocess_args.get(
            "default_tokens", [Tokens.PAD_TOKEN, Tokens.UNK_TOKEN])
        vocab_size = vocab_args.get("vocab_size", 0)

        data = dataset.data

        for field in fields:
            with Chained():
                data.loc[data.index, field] = PreProcessing\
                    .preprocess_data(data[field],
                                     preprocess_args,
                                     preprocess_fn)

        if build_vocab:

            vocab = None

            vocab_data = None
            vocab_field = vocab_args.get("field", "item")

            if not dataset.is_partitioned():
                logger.warning((f"The dataset is not partitioned"
                                f"The vocabulary is going to be built with the complete dataset"))
                vocab_data = dataset.data
            else:
                try:
                    vocab_data = dataset.get_partition(Partitions.TRAIN)
                except Exception as ex:
                    logger.exception(
                        (f"Dataset partition {Partitions.TRAIN} cannot be found."))
                    raise

            if vocab_builder:
                vocab = vocab_builder(vocab_data[vocab_field],
                                      vocab_args)
            else:
                vocab_builder = PreProcessing.build_vocab
                vocab = PreProcessing.build_vocab(vocab_data[vocab_field],
                                                  vocab_size=vocab_size,
                                                  def_tokens=def_tokens)

            assert vocab is not None and isinstance(vocab, list),\
                (f"The vocabulary was not properly built, if you choose a custom function to build it"
                 f"return a list of tokens")

            dataset.representations[TR.VOCAB] = vocab
            dataset.register_preprocessing_op(TR.VOCAB, {
                "vocab_size": len(vocab),
                "vocab_fields": vocab_field,
                "preprocessor": str(preprocess_fn),
                "vocab_builder": str(vocab_builder)
            })

        return dataset

    @staticmethod
    def build_vocab(data: Iterable,
                    vocab_size: int = 0,
                    def_tokens: list = [Tokens.PAD_TOKEN,
                                        Tokens.UNK_TOKEN]) -> List[str]:
        """
        Builds the vocabulary (optionally, limited to vocab_size most common words).
        It expects an iterable of list of words.

        Parameters
        ----------
        data : Iterable
            Data to process. It has to an iterable of list of words
        vocab_size : int, optional
            Limits the vocab length. If 0, vocab won't be limited, by default 0


        Returns
        -------
        List[str]
            List of words that represents the invverse_vocab `{ix:word}`

        """
        try:
            token_counter = Counter()
            vocab = def_tokens

            for sentence in data:
                sentence = sentence.split()
                for token in sentence:
                    token_counter[token] += 1

            if vocab_size > 0:
                token_counter = {k: v for
                                 k, v in token_counter.most_common(vocab_size - len(vocab))}

            vocab += list(token_counter.keys())

            return vocab
        except Exception as ex:
            logger.exception(ex)
            raise


class DefaultProcessor():
    translator = str.maketrans('', '', string.punctuation + '"“”')
    re_digits = re.compile(r'^\d+[\.\d]*([e|E]*[+-]*[\d]*)*\b')

    def __init__(self,
                 init_delimiter: str = "INIT_DELIMITER",
                 end_delimiter: str = "END_DELIMITER") -> None:
        self.init_delimiter = init_delimiter
        self.end_delimiter = end_delimiter

    def preprocess(self,
                   item: str,
                   preproc_args: dict = {
                       "delimit": False,
                       "remove_stop_words": False,
                       "only_alpha": False
                   }):
        """
        Default preprocessing method. 
        It contains options to delimit, to remove stop words and to keep only alpha words.


        Parameters
        ----------
        item : str, optional
            Sentence to preprocess
        preproc_args : dict, optional
            If delimit=True, it delimits sentences with self.init_delimiter and self.end_delimiter tokens.

            If only_alpha=True, if keep only alphabetic words, 

            by default 
            { 
            
                "delimit": False, 
            
                "remove_stop_words": False, 
            
                "only_alpha": False 
            
            }
        

        Returns
        -------
        [str]
            Sentence preprocessed
        """
        try:
            delimit = preproc_args.get("delimit")
            remove_stop_words = preproc_args.get("remove_stop_words")
            only_alpha = preproc_args.get("only_alpha")

            item = item.translate(self.translator)
            item = item.lower()

            if remove_stop_words:
                tokens = word_tokenize(item)
                tokens = [
                    word for word in tokens if word not in stopwords.words('english')]
                item = " ".join(tokens)

            if delimit:
                item = f"{self.init_delimiter} {item} {self.end_delimiter}"

            if only_alpha:
                item = [re.sub(self.re_digits, Tokens.NUM_TOKEN, t)
                        for t in item.split()]
                item = " ".join(item)

            return item
        except Exception as ex:
            logger.error(ex)
            log.error(traceback.print_exc())
            return ""