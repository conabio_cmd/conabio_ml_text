import multiprocessing
import traceback

from multiprocessing import Pool

from functools import partial

from typing import Callable, Iterable, Tuple

from conabio_ml.utils.logger import get_logger

logger = get_logger(__name__)

NUM_PROCESSES = multiprocessing.cpu_count() - 1


def poolify(data: Iterable,
            func_args: dict,
            fn: Callable,
            processes: int = NUM_PROCESSES):
    """
    Parallelizes a function on `processes` threads 

    Parameters
    ----------
    data : Iterable
        Data to parallelize
    func_args : dict
        Argument to be sent to the method
    fn : Callable
        Function to exectute
    processes : int, optional
        Number of processes, by default NUM_PROCESSES

    Returns
    -------
    obj
        Resulting data of `fn`
    """
    try:
        assert isinstance(data, Iterable), \
            (f"The object to poolify must be an Iterable and be able to parallelize using "
            f"multiprocessing")
            
        with Pool(processes=processes) as pool:
            func = partial(fn, func_args)
            data = pool.map(func, data)            

        return data
    except Exception as ex:
        logger.exception(ex)
        raise