"""
This script contains all the default contraints implemented for the conabio_ml_text library.
"""
#!/usr/bin/env python
# -*- coding: utf-8 -*-
class TransformRepresentations:
    """
    Implemented representations of text dataset
    """
    TF_DATASET = "dataset"
    DATA_GENERATORS = "data_generators"
    VOCAB = "vocab"


class LabelTypes:
    """
    Allowed types of labels to use in
    - Transform
    """
    CATEGORICAL = "categorical"
    ORDINAL = "ordinal"

class LearningRates:
    """
    Default implemented learning rate functions
    """
    EXPONENTIAL_LR = "exponential_lr"

class Optimizers:
    """
    Default implemented optimizers
    """
    ADAM = "Adam"