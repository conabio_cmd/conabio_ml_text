#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
    This script only expose utils for other processes
"""
import sys

import re
import requests
import string
import pydash
import unidecode
import traceback

from pathlib import Path

from conabio_ml_text.preprocessing.preprocessing import Tokens
from conabio_ml.utils.logger import get_logger

log = get_logger(__name__)

# region common utils for datasets

def download_dataset(url: str, dest_file: Path):
    """
    Downloads the dataset from a URL and stores into dest_file

    Parameters
    ----------
    url : str
        URL of the resource
    dest_file : Path
        Destination file to store in system
    """
    print(f"Fetching {url}")

    try:
        with open(dest_file, "wb") as f:
            response = requests.get(url, stream=True)
            total_length = response.headers.get('content-length')

            if total_length is None:  # no content length header
                f.write(response.content)
            else:
                dl = 0
                total_length = int(total_length)

                for data in response.iter_content(chunk_size=4096):
                    dl += len(data)
                    f.write(data)
                    done = int(50 * dl / total_length)
                    sys.stdout.write("\r[%s%s]%.1f%%" % (
                        '=' * done, ' ' * (50-done), dl*100. / total_length))
                    sys.stdout.flush()
    except Exception as ex:
        log.error(ex)
        log.error(traceback.print_exc())


def news_item_reader(filepath: str):
    """
    Since text documents can contain anything and could be possible you want to extract
    or process a section, this function is executed for each file in the dataset

    Parameters
    ----------
    filepath : str
        Source path of the file, as written in the `item` column

    Returns
    -------
    str
        Result of the processing
    """
    try:
        with open(filepath, encoding="ISO-8859-1", mode="r") as _f:
            newsfile = _f.read()

        start_index = newsfile.split("\n").index("")
        temp = pydash.chain(newsfile.split("\n")[start_index+1:])\
            .map(lambda x: x.strip().replace("\n", ""))\
            .filter(lambda x: len(x) > 0)\
            .value()

        return "\n".join(temp)
    except Exception as ex:
        log.error(ex)
        raise

# endregion

# region common utils for prerprocessing

def simple_item_preprocessing(item: str,
                    preproc_args: dict =
                    {
                        "remove_symbols": False,
                        "padding_size": 0
                    }) -> str:
    try:
        remove_symbols = preproc_args.get("remove_symbols", False)
        padding_size = preproc_args.get("padding_size", 0)

        tokens = []
        item = unidecode.unidecode(item)
        item = item.lower()

        if remove_symbols:
            item = re.sub(r'https?:\/\/.*[\r\n]*', '', item)

            item = item.translate(
                str.maketrans(string.punctuation, ' '*len(string.punctuation))
            )

        pad_token = Tokens.PAD_TOKEN

        if padding_size > 0:
            item = item.split()
            item_mask = (padding_size - len(item)) * [pad_token]

            item = item_mask + item[-padding_size:]
            item = " ".join(item)

        return item
    except Exception as ex:
        log.error(ex)
        raise ex

# endregion