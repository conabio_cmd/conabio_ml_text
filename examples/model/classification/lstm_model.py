#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
    This script use dataset and preprocessing pipelines, and does the following:

    1. Transform the dataset to tfkeras compatible type, we can use 
        datagenerator or tf.dataset
    2. Define an RNN inspired model for classification 
    3. Define the environment where the model will be trained 
    4. Train the model using a compatible conabio_ml.trainer
    5. Predict the results for the test partition
"""
import argparse
import os

import json

import traceback

from pathlib import Path

import tensorflow as tf
from tensorflow.keras import Model
from tensorflow.keras.layers import Dense, LSTM, Input, Embedding, Dropout, Bidirectional

# Remember to update the PYTHON_PATH to
# export PYTHONPATH=`pwd`:`pwd`/conabio_ml_text/conabio_ml:`pwd`/conabio_ml_text
from conabio_ml.pipeline import Pipeline

from conabio_ml_text.datasets.dataset import Dataset
from conabio_ml_text.preprocessing.transform import Transform
from conabio_ml_text.utils.constraints import TransformRepresentations as TR

from conabio_ml_text.trainers.bcknds.tfkeras import TFKerasBaseModel
from conabio_ml_text.trainers.bcknds.tfkeras_models import TFKerasRawDataModel

from conabio_ml_text.trainers.bcknds.tfkeras import TFKerasTrainer, TFKerasTrainerConfig
from conabio_ml_text.trainers.bcknds.tfkeras import CHECKPOINT_CALLBACK, TENSORBOARD_CALLBACK

# Prev preprocessing pipeline
from examples.preprocessing.simple_preprocessing import build_pipeline as build_prev

from conabio_ml.utils.logger import get_logger, debugger

log = get_logger(__name__)
debug = debugger.debug

# region model


class LSTMModel(TFKerasRawDataModel):

    @classmethod
    def create_model(cls,
                     layer_config: dict) -> TFKerasRawDataModel.TFKerasModelType:
        try:
            layers = layer_config["layers"]

            input_layer = layers["input"]
            embedding = layers["embedding"]
            lstm_1 = layers["lstm"]
            dense = layers["dense"]

            i = Input(shape=(input_layer["T"], ))
            x = Embedding(input_dim=embedding["V"],
                          output_dim=embedding["D"])(i)
            x = Bidirectional(
                LSTM(units=lstm_1["M"])
            )(x)
            x = Dropout(lstm_1["dropout"])(x)

            x = Dense(units=dense["K"],
                      activation="softmax")(x)  # Because we convert samples to one-hot

            model = Model(i, x)

            return model
        except Exception as ex:
            log.exception(ex)
            raise
# endregion

# region debug_info


def debug_option(dataset: Dataset.DatasetType) -> Dataset.DatasetType:
    # All the information related to the representations of the dataset is stored in
    # the `representations`  dict.
    # See the default dataset representations in utils->constaints->TransformRepresentations
    ds_transformed = dataset.representations[TR.TF_DATASET]
    train_transformed = ds_transformed["train"]

    transformed_sample = [*train_transformed.take(5).as_numpy_iterator()]
    print(transformed_sample)

    return dataset

# endregion


def build_pipeline(dest_path: Path,
                   pipeline_name: str,
                   config_file: Path) -> Pipeline:

    with open(config_file) as _f:
        config = json.load(_f)

    config_params = config["params"]
    model_layers = config["model_layers"]

    pipeline = build_prev(dest_path=dest_path,
                          pipeline_name=pipeline_name)
    # Till here we have a human readable dataset we need to convert
    # into backend model compatible type, since the model is based in tf-keras
    # we use a tf.dataset representation
    pipeline\
        .add_process(name="transform_dataset",
                     action=Transform.as_dataset,
                     inputs_from_processes=["preprocess_news_dataset"],
                     reportable=True,
                     args={
                         #  If you want to add a new vocab add the file here
                         #  Otherwise, to use the prev calculated vocab leave None
                         "vocab": None,
                         "categorical_labels": True,
                         "shuffle": True,
                         # You can change padding options and vocab size before
                         # training stage. If everything is correct just ommit this
                         # option
                         #   "transform_args": {
                         #       "pad_length": 40
                         #   }
                     })\
        .add_process(name="debug_transform",
                     action=debug_option,
                     inputs_from_processes=["transform_dataset"],
                     args={})\
        .add_process(name="trainer_config",
                     action=TFKerasTrainerConfig.create,
                     args={
                         "config": {
                             "strategy": None,
                             "callbacks": {
                                 CHECKPOINT_CALLBACK: {
                                     "filepath": os.path.join(dest_path, "checkpoints"),
                                     "save_best_only": True
                                 },
                                 TENSORBOARD_CALLBACK: {
                                     "log_dir": os.path.join(dest_path, "tb_logs")
                                 }
                             }
                         }
                     })\
        .add_process(name="create_classifier",
                     action=LSTMModel.create,
                     args={
                         "model_config": {
                             # Name of the model
                             "LSTM_CLASSIFIER": model_layers
                         }
                     })\
        .add_process(name="train_classifier",
                     action=TFKerasTrainer.train,
                     reportable=True,
                     inputs_from_processes=["transform_dataset",
                                            "create_classifier",
                                            "trainer_config"],
                     args={
                         "train_config": {
                             "LSTM_CLASSIFIER": {
                                 "representation": TR.TF_DATASET,
                                 'optimizer': tf.keras.optimizers.Adam(lr=config_params["initial_learning_rate"]),
                                 'loss': tf.keras.losses.CategoricalCrossentropy(),
                                 "epochs": config_params["epochs"],
                                 "metrics": ["accuracy"],
                                 "batch_size": config_params["batch_size"]
                             }}
                     })

    return pipeline


def run(dest_path: str,
        config_file: Path):
    dest_path = Path(dest_path)

    dest_path.mkdir(exist_ok=True)
    pipeline_name = "model_training"

    pipeline = build_pipeline(dest_path=dest_path,
                              pipeline_name=pipeline_name,
                              config_file=config_file)

    pipeline.run()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument("-dp", "--dest_path",
                        help="Destination path to store the results")
    parser.add_argument("-c", "--config_file",
                        help="Config file for the model params")
    parser.add_argument("-d", "--debug",  action='store_true',
                        help="Enables debug mode to trace the progress of your searching")
    parser.add_argument("-b", "--breakpoint",  action='store_true',
                        help="Enables breakpoint inside the debugger wrapper")

    ARGS = parser.parse_args()
    debugger.create(ARGS.debug, ARGS.breakpoint)

    dest_path = ARGS.dest_path
    assert dest_path, \
        "Destination path not set"

    config_file = ARGS.config_file
    assert Path(config_file).is_file(), \
        "Model configuration file not established"

    run(dest_path, config_file)
