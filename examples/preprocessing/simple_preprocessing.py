#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
    This script takes advantage of previously built pipelines, and does the following:
    
    1. Chains a pipeline that defines the dataset acquisition and split.
        ----
        ** Note we have to knows the name of previous processes to chain them
        ---

    2. Performs a simple preprocessing operations on text by adding a preprocessing in 
    the param `preprocess_fn` and `preprocess_args`
"""
import sys
import argparse

import traceback

from pathlib import Path
# Remember to update the PYTHON_PATH to
# export PYTHONPATH=`pwd`:`pwd`/conabio_ml_text/conabio_ml:`pwd`/conabio_ml_text
from conabio_ml.pipeline import Pipeline
from conabio_ml.assets import AssetTypes

from conabio_ml_text.datasets.dataset import Dataset
from conabio_ml_text.preprocessing.preprocessing import Tokens, PreProcessing

from conabio_ml_text.utils.constraints import TransformRepresentations as TR

# Pipeline for loading and splitting the dataset
from examples.dataset.news import build_pipeline as build_prev
from examples.utils import simple_item_preprocessing

from conabio_ml.utils.logger import get_logger, debugger

log = get_logger(__name__)
debug = debugger.debug


def report_vocab(dataset: Dataset.DatasetType,
                 dest_path: str,
                 process_args: dict):
    """
    ** Note we have to know the type of the process we are reporting

    We persist the vocabulary in the results folder for this stage
    and report to the pipeline by returning the object we create.

    Later, after finishing the pipeline execution we can read this asset in
    pipeline.json -> report -> stage_name -> vocab_file

    Parameters
    ----------
    dataset : Dataset.DatasetType
        Dateset used in the stage of this report function
    dest_path : str
        Destination path of the stage
    process_args : dict
        Args used in the prerprocess function
    """
    # All the information related to the representations of the dataset is stored in
    # the `representations`  dict. So, to persist the vocabulary we read
    # dataset.representations["vocab"] property
    # See the default dataset representations in utils->constaints->TransformRepresentations

    vocab = dataset.representations[TR.VOCAB]
    vocab_file = Path(dest_path)/"vocab"
    print(f"Vocab contains {len(vocab)} tokens")

    # Other representations such as transformations are stored in
    # `dataset.representations

    with open(vocab_file, mode="w") as _f:
        _f.write("\n".join(vocab))

    res = {
        "vocab": {"asset": vocab_file,
                  "type": AssetTypes.FILE}
    }
    return res





def build_pipeline(dest_path: Path,
                   pipeline_name: str) -> Pipeline:
    pipeline = build_prev(dest_path=dest_path,
                          pipeline_name=pipeline_name)

    # If the preprocessing function is simple and works only at
    # item level, we can override the default one and modify values for
    # vocabulary
    pipeline\
        .add_process(name="preprocess_news_dataset",
                     action=PreProcessing.preprocess,
                     inputs_from_processes=["20newsgroup_split"],
                     reportable=True,
                     report_functions=[report_vocab],
                     args={
                         "build_vocab": True,
                         "vocab_args":  {
                             "vocab_size": 5000
                         },
                         "preprocess_fn": simple_item_preprocessing,
                         "preprocess_args": {
                             "func_args": {
                                 "remove_symbols": True,
                                 "padding_size": 200
                             }}
                     })

    return pipeline


def run(dest_path: str):
    dest_path = Path(dest_path)

    dest_path.mkdir(exist_ok=True)
    pipeline_name = "dataset_preprocessing"

    pipeline = build_pipeline(dest_path=dest_path,
                              pipeline_name=pipeline_name)

    pipeline.run()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument("-dp", "--dest_path",
                        help="Destination path to store the results")
    parser.add_argument("-d", "--debug",  action='store_true',
                        help="Enables debug mode to trace the progress of your searching")
    parser.add_argument("-b", "--breakpoint",  action='store_true',
                        help="Enables breakpoint inside the debugger wrapper")

    ARGS = parser.parse_args()
    debugger.create(ARGS.debug, ARGS.breakpoint)

    dest_path = ARGS.dest_path
    assert dest_path, \
        "Destination path not set"

    run(dest_path)
