#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
    This script downloads the dataset 20 Newsgroup
    https://kdd.ics.uci.edu/databases/20newsgroups/20_newsgroups.tar.gz

    Then, process it from system folder using the conabio_ml_text
    library using a pipeline.

    It's primary intended to demonstrate:
    - the chain of pipeline stages
    - useful methods of conabio_ml_text.(…).Dataset
"""
import sys
import argparse
import pydash
import tarfile

import traceback

from pathlib import Path

# Remember to update the PYTHON_PATH to
# export PYTHONPATH=`pwd`:`pwd`/conabio_ml_text/conabio_ml:`pwd`/conabio_ml_text
from conabio_ml.pipeline import Pipeline

from conabio_ml_text.datasets import Dataset

from conabio_ml.utils.logger import get_logger, debugger

from examples.utils import download_dataset, news_item_reader
log = get_logger(__name__)
debug = debugger.debug



def build_pipeline(dest_path: Path,
                   pipeline_name: str) -> Pipeline:
    """
    Downloads the dataset and stores in system file.
    Then, it creates a dataset for further processing


    Parameters
    ----------
    dest_path : Path
        Destination path where pipeline will be stored
    pipeline_name : str
        Name of the pipeline

    Returns
    -------
    Pipeline
        Pipeline to execute
    """
    dataset_url = "https://kdd.ics.uci.edu/databases/20newsgroups/20_newsgroups.tar.gz"
    dest_dataset = dest_path / "20news.tar.gz"
    dataset_path = dest_path / "20_newsgroups"

    if not dest_dataset.is_file():
        download_dataset(dataset_url, dest_dataset)

    assert dest_dataset.exists(), \
        "Dataset tar file cannot be found"

    if not dataset_path.exists():
        print(f"Extracting dataset @{dest_path.absolute()}")

        tar = tarfile.open(dest_dataset)
        tar.extractall(path=dest_path)
        tar.close()

    assert dataset_path.exists(), \
        "Dataset folder cannot be found"

    pipeline = Pipeline(dest_path,
                        name=pipeline_name)\
        .add_process(name="20newsgroup_dataset",
                     action=Dataset.from_folder,
                     reportable=True,
                     args={
                         "source_path": dataset_path,
                         "recursive": True,
                         "extensions": ["txt"],
                         "split_by_folder": False,
                         "item_reader": news_item_reader,
                     })\
        .add_process(name="20newsgroup_split",
                     action=Dataset.split,
                     inputs_from_processes=["20newsgroup_dataset"],
                     reportable=True,
                     args={
                         "train_perc": 0.8,
                         "test_perc": 0.1,
                         "val_perc": 0.1,
                         "split_params": {
                             "random_state": 42
                         }
                     })

    return pipeline


def run(dest_path: str):
    """
    Builds the pipeline and executes it.

    Parameters
    ----------
    dest_path : str
        Destination path where pipeline will be stored
    """
    dest_path = Path(dest_path)

    dest_path.mkdir(exist_ok=True)
    pipeline_name = "get_news_dataset"

    pipeline = build_pipeline(dest_path=dest_path,
                              pipeline_name=pipeline_name)

    pipeline.run()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument("-dp", "--dest_path",
                        help="Destination path to store the results")
    parser.add_argument("-d", "--debug",  action='store_true',
                        help="Enables debug mode to trace the progress of your searching")
    parser.add_argument("-b", "--breakpoint",  action='store_true',
                        help="Enables breakpoint inside the debugger wrapper")

    ARGS = parser.parse_args()
    debugger.create(ARGS.debug, ARGS.breakpoint)

    dest_path = ARGS.dest_path
    assert dest_path, \
        "Destination path not set"

    run(dest_path)
