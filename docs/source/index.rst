
**conabio_ml_text** is an end-to-end extensible library that follows the same
ideology than the `conabio_ml <https://bitbucket.org/conabio_cmd/conabio_ml/src/master/>`_ library, 
but is intended to perform experiments over text data.

That means you will find common procedures related to text data managing. Nevertheless,
you can also implement your own methods according to your needs.

Like the `conabio_ml <https://bitbucket.org/conabio_cmd/conabio_ml/src/master/>`_ 
library, the main goal is to simplify the machine learning workflow by splitting the 
complete workflow into single substeps that can be reported and reproduced.

The components this library comprise are:

|  - dataset acquiring and processing
|  - model instantiation and training
|  - evaluation of the results



FEATURES
========

`conabio_ml_text` grounds on pipeline templating, stage reporting, 
and configuration to accomplish experiment traceability and reporting. 
It also, relies on model backend subclassing to extend the provided models.

.. toctree::
  :maxdepth: 2
 
  FEATURES <features>

MAIN COMPONENTS
================

The main components the `conabio_ml_text` relies on, are:

.. toctree::
  :maxdepth: 2
  :caption: COMPONENTS

  Dataset <components/dataset>
  Model <components/model>
  Trainer <components/trainer>

They work together to separate an ML experiment

.. toctree::
  :maxdepth: 2
  :caption: Component chain

  Chaining <components/chaining>



.. toctree::
  :maxdepth: 2
  :caption: INSTALLATION

  Package installation <installation/install>

MODELS
=======

The module `conabio_ml.trainer` is ready to add models for the available backends.
We provide you a guide for creating new models and use them in the library. 
We also show the usage and the configuration of available models.

.. toctree::
  :maxdepth: 2
 
  Model extension <models/model_extension>


API REFERENCE
=============

Links to the library modules:

.. toctree::
  :maxdepth: 2
 
  Packages <api_reference/modules>