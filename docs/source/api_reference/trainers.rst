trainers package
================

.. automodule:: trainers
   :members:
   :undoc-members:
   :show-inheritance:

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   trainers.bcknds

Submodules
----------

trainers.builders module
------------------------

.. automodule:: trainers.builders
   :members:
   :undoc-members:
   :show-inheritance:
