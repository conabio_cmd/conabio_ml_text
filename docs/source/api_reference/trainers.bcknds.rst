trainers.bcknds package
=======================

.. automodule:: trainers.bcknds
   :members:
   :undoc-members:
   :show-inheritance:

Submodules
----------

trainers.bcknds.tfkeras module
------------------------------

.. automodule:: trainers.bcknds.tfkeras
   :members:
   :undoc-members:
   :show-inheritance:

trainers.bcknds.tfkeras\_models module
--------------------------------------

.. automodule:: trainers.bcknds.tfkeras_models
   :members:
   :undoc-members:
   :show-inheritance:
