INSTALLATION
============

The conabio_ml_text library is still in the early stages of development. 
So, expect some changes while we publish an RC version.

For now, you can get the code from `the official bitbucket 
repo <https://bitbucket.org/conabio_cmd/conabio_ml_text>`_.


Requirements
-------------

This library has, as main requirement, the 
`conabio_ml <https://bitbucket.org/conabio_cmd/conabio_ml/src/master/>`_ library. 
To get the complete code, you also need to obtain the conabio_ml submodule. 
It can be achieved with the following instructions:

.. code-block:: shell

   git clone --recurse-submodules https://bitbucket.org/conabio_cmd/conabio_ml_text.git

Then, install the requirements for each library, with:

.. code-block:: shell

   pip install -r conabio_ml/requirements.txt requirements.txt


Environment
------------

To set up the environment, we suggest two options:

1. **Using docker**

    The easiest, and recommended way, to start with the `conabio_ml_text` 
    library is using the  `docker image <https://bitbucket.org/conabio_cmd/conabio_ml_text/src/master/images/tf2/Dockerfile>`_ 
    or the `docker-compose file <https://bitbucket.org/conabio_cmd/conabio_ml_text/src/master/docker-compose.yml>`_. 

    It has **Tensorflow 2.2.1** as backend and provide the complete environment. 
    You only have to define your code folder in the `volumes` param. As shown:

    .. code-block:: yaml
    
        volumes:
	        - ./your_path_here:/lib/code_environment/mapped_path_in_the_container

2. **Vía virtual environment**

    If you don't have a docker-ready environment, you can also get the code and run 
    it into a virtual environment. You only have to care for the `PYTHONPATH` 
    environment variable. Before using this library, remember to update the path with:

    .. code-block:: shell
    
        export PYTHONPATH=$PYTHONPATH:`pwd`:`pwd`/conabio_ml_text/conabio_ml:`pwd`/conabio_ml_text

    Note: *We are assumming, you are located in the project root path*.